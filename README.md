# User Feed module for CCPS Core apps

This module connects to a Grouper instance, for the purpose of generating a feed of user data (including status) to be ingested for other purposes by the application as a whole.

## Version to use

Current version requires **CCPS Core 3.0+**.

| **User Feed** | **CCPS Core** |
|---------------|---------------|
| 4.0+          | 3.0+          |
| 3.0+          | 2.0+          |
| 2.*           | 1.*           |

## Version

### 4.0

- Requires CCPS Core 3.0
- Views refactored to use TailwindCSS and CCPS Core Blade Components

### 3.0.1

- Requires Laravel legacy factories package to provide compatibility with Laravel 8+

### 3.0

- True semantic versioning
- Requires CCPS Core 2.0
- Cronjob return value tweaks according to Core 2.0 requirements
- Fixes missing `use` statements in the `UserFeedController` class
- Fixes some incorrect log channels
- Refactors `PruneOldDiffsAndBatches` job to use less memory / system resources
- Refactors cronjob default display names
- Adds GUI around Diffs and Batches
- Cast `metrics` property to `json` using Eloquent (stop using `metricsDecoded`)
- Remove old views for showing held data in favor of the new GUI for diffs/batches
- Refactors batch/diff actions onto those controllers instead of `HeldDataController`
- Slims down routes file considerably
- Fixes missing icon for `expanded` status

### 2.4.1

- PSR-4 fixes

### 2.4

- Removes references to functionality found in `owen-it/laravel-auditing`

### 2.3.1

- change to PSR-4 declaration since we were already following it

### 2.3

- Fetch retries introduced, because Grouper is proving to be a bit flaky. Can now specify `USER_FEED_FETCH_TRIES`

### 2.2.1

- Refines logic when ignoring fields - if changed fields array becomes empty because of ignored fields, we do not register it as a change.

### 2.2

- Adds ability to ignore fields when calculating changes. This will help with upgrade pains (can ignore `exists_in_filter` for example).

### 2.1.1

- Enables additional filtering by nonblank field(s)

### 2.1.0

- Expanding a user data batch now appends `mail` field instead of `uncgemail`, as that is the field that Grouper hands back implicitly.
- Allows configuration to drop the `mail` field, as including it suddenly will trigger changes on every user. *SEE BELOW IF UPGRADING FROM PREVIOUS VERSION!*

### 2.0

- Removal of LDAP filtering
- Namespace refactoring
- Facade refactoring
- Database field refactoring to remove JSON requirement from columns that may be encrypted
- removes dependency on LDAP package
- fixes an "undefined index" bug that would occur when the Grouper API return an empty resultset for a requested group

### 1.1.1

- Fix for typo in `withSubjectAttributeNames()`

### 1.1.0

- Change method to retrieve Grouper member list away from flattened member tree (in response to Grouper account permission changes) - member tree is no longer reliable when not using accounts with all-access to Grouper. Instead, `getAllMembers()` is used, and entries identified as groups are parsed out.

### 1.0.7

- Laravel 5.8 support (removes references to `str_plural` global helper)

### 1.0.6

- Fixes a bug with the fallback for a field that exists in an old user data object but not a new; the intent was for the field to be set to `null` in the new array, but instead the double-equals was used, which caused a fatal error with the diff process in these cases.

### 1.0.5

- Fixes a bug that resulted in requested LDAP fields not being merged into `UserData` object along with Grouper data

### 1.0.4

- Fall back to empty string if an attribute requested from LDAP is blank or not set for a user.

### 1.0.3

- Applies PHP overrides to all cronjobs, not just `FetchNewUserDataBatch`

### 1.0.2

- Add logging to PHP overrides from 1.0.1

### 1.0.1

- Add overrides to bypass memory / exec time limits in PHP temporarily during `FetchNewUserDataBatch` cronjob

### 1.0.0

- Initial launch!

---

# Installation

## Prerequisites

You should have a base application created with [CCPS Core](https://bitbucket.org/uncg-its/ccps-core/src).

## Preparation

This package and its requirements are not on Packagist. You will need to alter your `repositories` object in `composer.json` to ensure that it includes the following repositories:

```
"repositories": [
    {
      "type": "git",
      "url": "https://bitbucket.org/uncg-its/ccps-user-feed"
    },
    {
      "type": "git",
      "url": "https://bitbucket.org/uncg-its/grouper-api-wrapper-laravel"
    },
    {
      "type": "git",
      "url": "https://bitbucket.org/uncg-its/grouper-api-php-library"
    }
  ],
```

## Installing the package

Run `composer require uncgits/ccps-user-feed` to install this package.

## Complete the installation

After installing the package, run `php artisan migrate` to generate the tables, cronjob data, and roles/permissions for this module.

In your `.env` file, add the following fields:

```
# Grouper
GROUPER_API_HOST=
GROUPER_API_USERNAME=
GROUPER_API_PASSWORD=

GROUPER_API_SUBJECT_ATTRIBUTE_NAMES= # default attribute names that will be used with withSubjectAttributeNames() and withSubjectAttributeNamesString()
GROUPER_API_PERSON_IDENTIFIERS= # source id strings that identify a person
GROUPER_API_GROUP_IDENTIFIERS= # source id strings that identify a group

# User Feed
GROUPER_SOURCE_GROUPS= # comma-separated
GROUPER_ENTITY_ID_FIELD=
GROUPER_USER_FEED_FIELDS= # optional. comma-separated, defaults to value in GROUPER_API_SUBJECT_ATTRIBUTE_NAMES if omitted

ENCRYPT_USER_DATA=true

DIFFS_TO_KEEP=1
```

## Grouper API setup

Ensure that you follow the steps in the Grouper API Wrapper setup: https://bitbucket.org/uncg-its/grouper-api-wrapper-laravel/src/master/ - at the least, you need to ensure that `grouper-api.php` is copied to your `config` folder, so a `php artisan vendor:publish` may be in order to help with that.

## CCPS Core Module Registration

After installing and configuring the basics of the application, register this module into the CCPS Core application by the standard method (the `'modules'` array in `config/ccps.config`). Here is a suggested configuration array:

```php
'user-feed' => [
    'package'              => 'uncgits/ccps-user-feed',
    'icon'                 => 'fas fa-user',
    'title'                => 'User Feed',
    'index'                => 'user-feed.index',
    'parent'               => '',
    'required_permissions' => 'user-feed.*',
    'use_custom_routes'    => false,
    'custom_view_path'     => 'ccps-user-feed::',
],
```

## Optional - publish configuration

If you wish, you can publish the config file (not required): `php artisan vendor:publish`, and then select the `Provider: Uncgits\Ccps\UserFeed\UserFeedServiceProvider` option.

## Optional - configure logging

You may add a log channel to your `config/logging.php` file if you wish:

```php
'user-feed' => [
    'driver' => 'daily',
    'path' => storage_path('logs/user-feed.log'),
    'level' => 'debug'
],
```

By default this module logs to the `stack` channel, but you may also add `USER_FEED_LOG_CHANNEL` to your `.env` file to override this.

## Optional - increase PHP memory limit and execution time

Depending on the size of your queries, you may run into PHP memory or execution time limits during one or more of the cronjobs for this package. You can optionally add one or both of the following to your `.env` file to override this:

```
USER_FEED_MEMORY_LIMIT=256M ; use a valid php.ini value
USER_FEED_MAX_EXECUTION_TIME=600 ; use a valid php.ini value
```

---

# Setup

### Defining the Source

Your user data batch "source" is derived from one or more Grouper groups. In the `.env` file, you should first complete the Grouper API fields according to the documentation on the `uncgits/grouper-api-wrapper-laravel` [package](https://bitbucket.org/uncg-its/grouper-api-wrapper-laravel/src/master/). Then, complete the following additional fields:

* `GROUPER_SOURCE_GROUPS` should be a comma-separated list of Grouper groups, including stem. Example: `uncg:sample:group1`, or `uncg:sample:group1,uncg:sample:group2`

* `GROUPER_ENTITY_ID` should be the name of a field from Grouper that will be used as the "entity ID", or primary identifier for a user. Ideally this field is an ID number of sorts, that does not have any meaning on its own (for security purposes, as it will be stored in the database in the clear). Example: `uncgidnumber`

* `GROUPER_USER_FEED_FIELDS` is an *optional* field you can add, that will be used to determine which attributes a user record includes. If omitted, this will default to the value from the API package (`GROUPER_API_SUBJECT_ATTRIBUTE_NAMES`). The fields for the User Feed are able to be configured separately so that they do not interfere with other places in the application where you may be using the Grouper API Library.

### Dropping fields

Grouper, by default, always includes the `mail` field, even if it was not explicitly requested. This was identified in version 2.1.0, and properly added to the expansion process - however, this propagated the need to be able to ignore / drop fields, as suddenly adding a field to the User Feed process will trigger all users as changed. While not implicitly a bad thing, downstream effects are unknown - and so as a failsafe, the ability to drop fields was added.

In the `.env` file, set the `USER_FEED_DROPPED_FIELDS` to a comma-separated list of fields you wish to ignore. Likely, if upgrading, you will want to add `mail` to this list. During expansion of the user data from the Batch, these fields will be excluded - as if they never existed in the source.

It is likely you will not need to use this much - if at all - outside of this `mail` context, but because we rely on Grouper (whose configuration may change), we cannot predict for sure.

### Filtering blank fields

Most of your filtering should be done at the source (Grouper) using "group math." However, when this is not available, this package offers a very rudimentary mechanism to allow you to exclude records from the Batch if specified fields are blank.

To add a comma-separated list of field names that must not be blank coming from Grouper, add the `USER_FEED_REQUIRED_NONBLANK_FIELDS` value to your `.env` file. For example:

```
USER_FEED_REQUIRED_NONBLANK_FIELDS=firstname,lastname
```

> Important! This method will ignore the record from the Batch entirely! This is not recommended to be used for "data-quality concerns" - it should only be used when you know that there is a certain field you want to be present on each user record for it to be inlcuded in the Batch.

### Ignoring fields

While not something that will likely be necessary long-term, sometimes you may need to ignore changes to a field for a run or two of the feed. This would be most common when upgrading to the 2.x version of the feed from 1.x - you can ignore `exists_in_filter` so that changes aren't triggered on all users.

To add a comma-separated list of field names to ignore when calculating changed user data entries, add the `USER_FEED_IGNORED_FIELDS` value to your `.env` file. For example:

```
USER_FEED_IGNORED_FIELDS=exists_in_filter
```

### Fetch retries

Occasionally, the Grouper API may not respond as expected when asking for the population of a group. In these cases, the result array - keyed by group name - will omit the requested group key. In previous versions of the User Feed the code would have a failsafe and use an empty array instead, and require the user of the Feed to have Alarms in place to catch the anomaly.

However, it was determined that a "retry" mechanism might be a better pre-emptive solution. By default the Feed will try the Fetch operation up to 3 times, retrying automatically if the requested group key was not found in the result array.

You can change how many attempts are made in the `.env` file - for instance:

```
USER_FEED_FETCH_TRIES=5
```

If no result is returned after the limit is hit, an exception will be thrown and the job will fail.

---

# Basic Usage

After running the migrations and setting up the `.env` variables appropriately for your application, you should see five new Cronjobs in the Cronjob panel (tagged with the `ccps-user-feed` tag): `FetchNewUserDataBatch`, `ExpandUserDataFromBatch`, `CreateUserDataDiff`, `ExpandUserDataChangesFromDiff`, and `PruneOldDiffsAndBatches`. Set the schedule and other metadata on these cronjobs using the GUI, and then enable these Cronjobs. Ensure that the jobs run in that order - and that there is sufficient time in between the different jobs for both the job to complete, and also for whatever other "cushion" time you want for padding. The general recommendation is for these jobs to run at least 15 minutes apart.

That's it! Your user data should be fetched, parsed, and diffed, with the results appearing in the `ccps_userfeed_user_data_changes` table.

---

# Alarms

It is important to ensure that the user data that you are processing meets your expectations, and also important for you to be able to take appropriate action when it does not.

**Alarms** are the way to accomplish this with the User Feed. Alarms are classes, written by the application developer, and stored in the app code. After various User Feed cronjobs are run, the data from the cronjob is passed to a method that iterates through any configured Alarm classes configured to "watch" that cronjob.

## Creating and writing a new Alarm class

Use `php artisan ccps:make:alarm` to create a new Alarm class from the stub. This class will be saved to the folder defined in the `ccps-user-feed.php` config file (if published), and defaults to `app/Alarms`. If changed, note that *all* Alarms must reside in this folder in order to be picked up by the app (e.g. there is no support for multiple Alarm folders).

Alarms are conceived with a pass/fail mechanic. Details on the method structure are below, but in general, use present-tense, affirmative nomenclature when naming the class. For example, a good name would be `ContainsExpectedNumberOfSourceRecords`. A poor name would be `NotEnoughSourceRecords`. The actual naming does not affect the operation of the class or mechanic at all, but the concept is that an Alarm check should "pass" when the desired end is achieved, and "fail" when it does not.

### Determining success or failure.

Each Alarm has a `passes()` method, which returns a boolean value on whether the check does, in fact, pass. So, your `passes()` method should return `true` if the check passes, or `false` if the check fails.

### Data available in an Alarm class

The Alarm class will be fed data by the parent cronjob by way of the `$data` argument. You should check the source of each cronjob in this package to see exactly what will be contained in this variable, as it varies per cronjob, but here is a general idea:

`FetchNewUserDataBatch` - the `Batch` object is passed in, from which the `metrics` field may be most useful. A `Batch` object is structured as follows:
```php
[
    'metrics' => json_encode([
        'source_records_count'  => 1, // integer
        'source_fields'         => [], // array
        'memory_used_mib'       => 0, // integer, representing peak MiB of memory used during the transaction.
        'time_taken_in_seconds' => 0.0 // integer, number of seconds the operation took to complete
    ]),
    'data'    => [
        'source' => [], // array
    ],
    'status'  => 'pending' // string,
];
```

`ExpandUserDataFromBatch` - an array will be passed, including the source `Batch` object and another array of entity ID fields that were successfully expanded:
```php
[
    'batch' => $batch, // Batch object
    'idsExpanded' => [] // array
]
```

`CreateUserDataDiff` - the generated `UserDataDiff` object will be passed in, from which the `metrics` field may again be most useful. Its structure is very similar to a `Batch`

`ExpandUserDataChangesFromDiff` - an array will be passed in, including the source `UserDataDiff` object and another array of `UserDataChange` objects generated during the expansion:
```php
[
    'userDataDiff'   => $diffToExpand, // UserDataDiff object
    'createdChanges' => [] // array of UserDataChange objects
]
```

`PruneOldDiffsAndBatches` - an array with Collections of both the `UserDataDiff`(s) and the `Batch`(es) queued for deletion:

```php
[
    'diffsToRemove'   => [], // Collection
    'batchesToRemove' => [], // Collection
]
```

### Taking action

There are multiple ways to act upon the results of an Alarm. By default, the only action that takes place when an Alarm check fails is a Log notification (channel and severity is defined when registering the Alarm, below). The developer, therefore, needs to decide what other actions will occur on both a passing result and a failing result. This can be done with either the built-in `onPass()` and `onFail()` methods on the Alarm class, or with Events and Listeners.

**What actions should I consider taking??**

The actions you take are largely going to depend on what the consequences of a failed (or passed) alarm check should be. Each cronjob with this package is wrapped with in a database transaction - that is, before any database changes are initiated, `DB::beginTransaction()` is called. And, after the Alarm check is completed, `DB::commit()` is called. If a failure of your Alarm means you want to _prevent the database changes from taking effect_, simply call `DB::rollBack()` as part of your `onFail()` method (or Listener class).

Other things to consider:

* Sending alerts
* stopping cronjobs (see more below)
* disabling other pieces of the application
* locking the application from new logins

You get the idea. The idea is to give the developer both the *ability* to control as much as possible, but also the *responsibility* to take whatever action is necessary. This module cannot presume to know everything that you want to happen when an alarm fails.

#### Using the built-in methods

Each `Alarm` object will call the `onPass()` or `onFail()` method when the alarm check passes or fails respectively. In the base class, these methods are empty. So, to use them in your `Alarm` object, simply define them. Each should take `$data` as its only argument (refer to the abstract parent class). For example:

```php
public function onFail($data) {
    DB::rollBack();
    sendNotificationsToAdmins();
    stopCronjobs();
    otherThings();
}
```

#### Listeners

If you choose, you can use Listeners instead of the built-in methods - perhaps the asynchronous approach is more appropriate for your application or coding style, or you prefer a more modular approach (especially if you may end up repeating code in multiple `onFail()` methods across several Alarms). When the Alarm check passes, an instance of `\Uncgits\Ccps\UserFeed\Events\UserFeedAlarmCheckPassed` is dispatched. Likewise, upon failure, an instance of `\Uncgits\Ccps\UserFeed\Events\UserFeedAlarmCheckFailed` is dispatched. Each event will be given three properties:

```php
public $alarmClass; // the class of the Alarm triggering this Event
public $data; // the $data variable from the Alarm
public $cronjobClass; // the class of the Cronjob from which the Alarm was triggered
```

Simply write and register your Listener as normal, bound to one of these events, and take action there as in the above example for using the built-in methods. In these cases it would probably help to do a check inside the Listener on `$alarmClass` to ensure you're reacting to the right Alarm (`$alarmClass`), and maybe even do the same for `$cronjobClass`.

### Additional Alarm options

Each Alarm class contains a couple of useful properties and methods that may help cut down on the work you do when you need to stop one or more cronjobs as the result of a failure (or success) of an Alarm check.

Inside each Alarm class is a `$cronjobClassesToStop` property, which is filled in by default with the names of each CCPS User Feed cronjob, commented out.

In your alarm, you may uncomment individual job names in the `$cronjobClassesToStop` property, or add your own:

```php

protected $cronjobClassesToStop = [
    // class names here:
    '\Uncgits\Ccps\UserFeed\Cronjobs\FetchNewUserDataBatch`, // string syntax
    ExpandUserDataFromBatch::class // ::class syntax (with appropriate use statement)
];

```

> Using `::class` is generally recommended to ensure that you do not have any typos in your class names. It makes no difference in functionality, however.

Then, calling `$this->stopCronjobs()` in your `onFail()` method, would stop your defined list of cronjobs. (Each Alarm class extends the `BaseAlarm` class, which contains the `stopCronjobs()` method.)

## Registering the Alarm

Once you have written your Alarm class, you need to register it in the application database before it can be used. The GUI provides a way to do this in the 'User Feed' menu item, but if you would like, you can write a migration / seed script to generate registrations that will be a part of your application by default. Navigate to "User Feed" > "Alarms" to see all current Alarm registrations.

Creating a new Alarm registration will provide you the opportunity to fill in multiple values about this Alarm:

- The display name (in your application) for the Alarm.
- The Alarm Class - the name of the file/class generated in the previous section
- Cronjob Class - which cronjob will pass its data to this Alarm
- Active? - is this Alarm on or off?
- Log Level - all failures for this Alarm will be logged at this level
- Log Channel - all failures for this Alarm will be logged in this channel

---

# Pruning old data

Stored user data and diffs can be quite large, as they likely contain thousands (maybe tens of thousands) of records. Thus, it is (typically) not prudent to keep generated data any longer than necessary. The `PruneOldDiffsAndBatches` cronjob helps you to manage this data effectively, as it continuously checks to make sure old / stale is deleted.

This cronjob determines what qualifies as "old / stale" simply through a configuration variable you define in the `.env` file with the `DIFFS_TO_KEEP` key. The number you specify here must be greater than 0, and will be consulted when the cronjob runs. When the cronjob detects more than the number of diffs you have specified, it will automatically remove the oldest diff, its change data, and the old batch (not currently being referenced by another diff). For example, if you set `DIFFS_TO_KEEP=3`, then the cronjob will start this process when the fourth diff is fully processed.

> Note: only diffs with a status of 'expanded' will be considered in this operation. This cronjob will never prune diffs with any other status ('held', 'ignored', 'approved', etc.)

**When this job removes data, it is permanently removed. Soft Deletes are not used. Be sure to take appropriate backups of your database if you are concerned about losing old data!**

---

# Using data from the User Feed in your application

## Primary objective: show what changed

The "end-result" of this module is to provide the `ccps_userfeed_user_data_changes` table with the most up-to-date information on what has changed about the user population you define. So, most often you will simply reference this information.

** HOWEVER... **

You may be tempted to simply use `UserDataChange::` queries to determine which are the most recent changes. This is not 100% correct. Consider: the User Feed can store data from many different diffs at once. If you are setting the application's `DIFFS_TO_KEEP` to a value above 1, you will see multiple sets of `UserDataChange` records in the `ccps_userfeed_user_data_changes` table. Thus, if a diff results in zero changes to any users, querying the `UserDataChange` model for the latest-timestamped data would then provide you with the results of the *last diff to generate any changes*. Now, this may not end up affecting your application at all, but it is more accurate / "truthful" to get the latest change data *by way of the latest expanded diff*. There are two built-in shortcuts here (via query scopes) that you can use to do this most effectively:

### Get the latest expanded Diff with its changes attached via model relationship

Use `UserDataDiff::latestExpanded()` to get the last-expanded diff with the `UserDataChange` records attached via the `user_data_changes` relationship property. Thus, to iterate through the changes for that diff, you could simply references them through `UserDataDiff::latestExpanded()->user_data_changes`, and do things like:

```php
UserDataDiff::latestExpanded()->user_data_changes->each(function($item) {
    // do things with $item here to affect your application
});
```

### Directly access the changes from the latest expanded Diff

The above process is made one step shorter through the shortcut `UserDataDiff::latestChanges()`:

```php
UserDataDiff::latestChanges()->each(function($item) {
    // do things with $item here to affect your application
});
```

The potential downside here is that you do not get any information about the Diff itself, but it is a quicker, cleaner way to get just the changes from the latest diff with 'expanded' status.

## Secondary objective: provide latest data on all users on-demand

A potential benefit of the User Feed's methodology is that you have the latest user data stored in the database, available to consult as needed. You will of course be limited by the fields you asked for from Grouper, but you are still able to reference this data via the `UserData` model. Like with the `Diff` and `UserDataChange` models, this is best done by going through the `Batch` model.

### Get the latest expanded Batch with its data attached via model relationship

You can use the `Batch` class similarly to the `UserDataDiff` class, including `Batch::latestExpanded()` - and then access the data via `->user_data`.

### Get the latest diffed Batch with its data attached via model relationship

In a similar fashion, you can get the latest `Batch` with the status of 'diffed' via `Batch::latestDiffed()`, and then access the data via `->user_data`.

### Directly access the data from the latest expanded Batch

`Batch::latestData()` is a shortcut to give you data from the latest expanded Batch.

---

# Managing Held Data

When something goes wrong, a `Batch` or `Diff` should be set with a status of 'held'. In this case, many of the built-in cronjobs will not continue to function (e.g. fetching a new batch will not work when the application detects at least one `Batch` with status of 'held'; same with `Diff`s.)

**A status of 'held' requires manual intervention to fix.** This is by design. It is recommended to ensure that you are either including notifications in the Alarm classes you write whenever you set a `Batch` or `Diff` to 'held', or implementing another mechanism to check for 'held' items regularly and trigger notifications. The User Feed will not continue to function normally when there is 'held' data, and so the consumer application will not be receiving updated user data.

## GUI toggles

In your CCPS Core application, the User Feed menu provides an easy way to inspect and deal with any Held Data. Go to "User Feed" > "Manage Held Data" to be shown a listing of held `Batch` or `Diff` objects. You can see details on each of these items, and change status to either 'approved' or 'ignored' here as well.

## API

A `Batch`'s status may also be set programmatically, preferably via the built-in methods `approveBatch()`, `ignoreBatch()`, or `holdBatch()`. Similarly, a `Diff`'s status may be set programmatically via `approveDiff()`, `ignoreDiff()`, or `holdDiff()`.

---

# Testing

This package is not yet set up for testing. :(

Hopefully a future version will incorporate tests. Help always accepted :)

---


# Questions?

[ccps-developers-l@uncg.edu](mailto:ccps-developers-l@uncg.edu)

[https://bitbucket.org/uncg-its/ccps-core](https://bitbucket.org/uncg-its/ccps-core)

---

# Version History

## 4.1.0

- Open Source BSD license

## Prior...

See tags... will flesh out someday.
