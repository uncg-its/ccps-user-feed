<?php

namespace Uncgits\Ccps\UserFeed\Seeders;

use App\CcpsCore\Role;
use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;

class UserFeedRoleSeeder extends CcpsValidatedSeeder
{
    // Sample roles seeding. The structure is checked against what is defined in the parent class ($roleArrayConstruction)

    public $roles = [
        [
            "name"         => "user-feed.viewer",
            "display_name" => "User Feed - Viewer",
            "description"  => "Can view information pertaining to the User Feed",
        ],
        [
            "name"         => "user-feed.editor",
            "display_name" => "User Feed - Editor",
            "description"  => "Can modify information pertaining to the User Feed"
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = \App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {

            // checks the data against the defined data structure.
            $this->validateSeedData($this->roles, $this->roleArrayConstruction);
            // checks to be sure that data doesn't already exist in the table
            $this->checkForExistingSeedData($this->roles, Role::all());

            // essential data that should be merged with each record before seeding
            $mergeData = [
                'source_package' => 'uncgits/ccps-user-feed',
                'created_at'     => date("Y-m-d H:i:s", time()),
                'updated_at'     => date("Y-m-d H:i:s", time()),
                'editable'       => 0
            ];

            $this->commitSeedData($this->roles, 'ccps_roles', $mergeData);
        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
