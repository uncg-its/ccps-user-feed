<?php

namespace Uncgits\Ccps\UserFeed\Seeders;

use App\CcpsCore\Permission;
use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;

class UserFeedPermissionSeeder extends CcpsValidatedSeeder
{
    // Sample permissions seeding. The structure is checked against what is defined in the parent class ($permissionArrayConstruction)

    public $permissions = [
        [
            "name"         => "user-feed.view",
            "display_name" => "User Feed - View",
            "description"  => "Can see information pertaining to the User Feed",
        ],
        [
            "name"         => "user-feed.edit",
            "display_name" => "User Feed - Edit",
            "description"  => "Can edit information pertaining to the User Feed"
        ],
        [
            "name"         => "user-feed.delete",
            "display_name" => "User Feed - Delete",
            "description"  => "Can delete information pertaining to the User Feed"
        ],
        [
            "name"         => "user-feed.create",
            "display_name" => "User Feed - Create",
            "description"  => "Can create information pertaining to the User Feed"
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = \App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {

            // checks the data against the defined data structure.
            $this->validateSeedData($this->permissions, $this->permissionArrayConstruction);
            // checks to be sure that data doesn't already exist in the table
            $this->checkForExistingSeedData($this->permissions, Permission::all());

            // essential data that should be merged with each record before seeding
            $mergeData = [
                'source_package' => 'uncgits/ccps-user-feed',
                'created_at'     => date("Y-m-d H:i:s", time()),
                'updated_at'     => date("Y-m-d H:i:s", time()),
                'editable'       => 0
            ];

            $this->commitSeedData($this->permissions, 'ccps_permissions', $mergeData);
        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
