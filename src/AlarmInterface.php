<?php


namespace Uncgits\Ccps\UserFeed;

interface AlarmInterface
{
    function passes($data);

    function onPass($data);

    function onFail($data);
}