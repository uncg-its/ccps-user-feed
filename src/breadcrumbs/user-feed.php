<?php

/*

Copy example below as a template, replace info in CAPS.
This will be pulled in automatically via CCPS module definitions

- Top level:

Breadcrumbs::register('NAME', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent($module['parent'] ?: 'home');
    $breadcrumbs->push("TITLE", route('ROUTENAME'));
});

- Children:

Breadcrumbs::register('NAME', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('PARENTNAME');
    $breadcrumbs->push("TITLE", route('ROUTENAME'));
});

*/
Breadcrumbs::register('user-feed', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent($module['parent'] ?: 'home');
    $breadcrumbs->push("User Feed", route('user-feed.index'));
});


Breadcrumbs::register('user-feed.alarms', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('user-feed');
    $breadcrumbs->push("Alarms", route('user-feed.alarms.index'));
});

Breadcrumbs::register('user-feed.alarms.show', function ($breadcrumbs, $alarm) use ($module) {
    $breadcrumbs->parent('user-feed.alarms');
    $breadcrumbs->push($alarm->name, route('user-feed.alarms.show', $alarm->id));
});

Breadcrumbs::register('user-feed.alarms.create', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('user-feed.alarms');
    $breadcrumbs->push("Create", route('user-feed.alarms.create'));
});

Breadcrumbs::register('user-feed.alarms.edit', function ($breadcrumbs, $alarm) use ($module) {
    $breadcrumbs->parent('user-feed.alarms');
    $breadcrumbs->push($alarm->name, route('user-feed.alarms.show', $alarm->id));
    $breadcrumbs->push("Edit", route('user-feed.alarms.edit', $alarm->id));
});


Breadcrumbs::register('user-feed.held-data', function ($breadcrumbs) use ($module) {
    $breadcrumbs->parent('user-feed');
    $breadcrumbs->push('Held Data', route('user-feed.held-data.index'));
});

Breadcrumbs::register('user-feed.batches', function ($breadcrumbs) {
    $breadcrumbs->parent('user-feed');
    $breadcrumbs->push('Batches', route('user-feed.batches.index'));
});
Breadcrumbs::register('user-feed.batches.show', function ($breadcrumbs, $batch) {
    $breadcrumbs->parent('user-feed.batches');
    $breadcrumbs->push($batch->id, route('user-feed.batches.show', $batch));
});

Breadcrumbs::register('user-feed.diffs', function ($breadcrumbs) {
    $breadcrumbs->parent('user-feed');
    $breadcrumbs->push('Diffs', route('user-feed.diffs.index'));
});
Breadcrumbs::register('user-feed.diffs.show', function ($breadcrumbs, $diff) {
    $breadcrumbs->parent('user-feed.diffs');
    $breadcrumbs->push($diff->id, route('user-feed.diffs.show', $diff));
});
