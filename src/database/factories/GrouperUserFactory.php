<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Uncgits\Ccps\UserFeed\GrouperUser::class, function (Faker $faker) {

    // get basics
    $firstName = $faker->firstName;
    $firstInitial = substr($firstName, 0, 1);

    $middleName = $faker->firstName;
    $middleInitial = substr($middleName, 0, 1);

    $lastName = $faker->lastName;
    $lastNameEmail = (strlen($lastName) > 6) ? substr($lastName, 0, 5) . $faker->randomDigit : $lastName;

    $displayName = $firstName . ' ' . $middleInitial . ' ' . $lastName;

    $username = strtoupper($firstInitial . $middleInitial . $lastNameEmail);
    $email = strtolower($username . '@uncg.edu');

    $id = $faker->randomNumber(6);

    $subjectAttributeFakerMap = [
        'uncgpreferredsurname'   => 'var:lastName',
        'uncgpreferredgivenname' => 'var:firstName',
        'cn'                     => 'var:username',
        'uncgemail'              => 'var:email',
        'departmentnumber'       => 'randomNumber:5',
        'uidnumber'              => 'var:id',
        'dn'                     => 'raw:CN=' . $username . ',ou=accounts,dc=devauth,dc=uncg,dc=edu',
        'displayname'            => 'var:displayName',
        'department'             => 'word',
        'employeeid'             => 'randomNumber:9'
    ];


    $subjectAttributeNames = config('grouper-api.subject_attribute_names');
    if (!in_array('uncgemail', $subjectAttributeNames)) {
        // Grouper always includes this whether or not it was requested
        $subjectAttributeNames = array_merge($subjectAttributeNames, ['uncgemail']);
    }
    $attributeArray = [];
    foreach ($subjectAttributeNames as $attr) {
        if (!isset($subjectAttributeFakerMap[$attr])) {
            throw new \Exception('Error: no Faker mapping for subject attribute name: ' . $attr);
        }

        $fakerMethod = $subjectAttributeFakerMap[$attr];

        $commandParts = explode(':', $fakerMethod);
        $command = $commandParts[0];

        if ($command == 'raw') {
            $fakerValue = $commandParts[1];
        } else {
            if ($command = 'var') {
                $varName = $commandParts[1];
                $fakerValue = $$varName;
            } else {
                if (count($commandParts) == 2) {
                    $params = explode(',', $commandParts);
                    $fakerValue = call_user_func([$faker, $command], $params);
                } else {
                    if (count($commandParts) == 1) {
                        $fakerValue = call_user_func([$faker, $command]);
                    }
                }
            }
        }

        $attributeArray[$attr] = $fakerValue;
    }

    return [
        'attributeValues' => $attributeArray,
        'id'              => $id,
        'name'            => $displayName,
        'resultCode'      => 'SUCCESS',
        'sourceId'        => 'uncg-person',
        'success'         => 'T'
    ];


});
