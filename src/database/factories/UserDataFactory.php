<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Uncgits\Ccps\UserFeed\UserData::class, function (Faker $faker) {
    return [
        'batch_id'         => $faker->randomNumber(10),
        'entity_id'        => $faker->randomNumber(6),
        'data'             => json_encode([
            'first_name' => $faker->firstName,
            'last_name'  => $faker->lastName,
            'email'      => $faker->email
        ])
    ];
});
