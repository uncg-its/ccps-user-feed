<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccps_userfeed_batches', function (Blueprint $table) {
            $table->uuid('id');
            $table->json('metrics');
            $table->json('data');
            $table->boolean('encrypted');
            $table->enum('status', ['pending', 'approved', 'expanded', 'diffed', 'held', 'ignored']);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccps_userfeed_batches');
    }
}
