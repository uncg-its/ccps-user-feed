<?php

use App\CcpsCore\User;
use App\CcpsCore\Permission;
use App\CcpsCore\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class SeedUserfeedRolesAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // roles
        Artisan::call('db:seed', [
            '--class' => 'Uncgits\\Ccps\\UserFeed\\Seeders\\UserFeedRoleSeeder',
            '--force' => true
        ]);

        // permissions
        Artisan::call('db:seed', [
            '--class' => 'Uncgits\\Ccps\\UserFeed\\Seeders\\UserFeedPermissionSeeder',
            '--force' => true
        ]);

        // permission-role
        Artisan::call('db:seed', [
            '--class' => 'Uncgits\\Ccps\\UserFeed\\Seeders\\UserFeedPermissionRoleTableSeeder',
            '--force' => true
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permissions = Permission::whereIn('name',
            ['user-feed.view', 'user-feed.edit', 'user-feed.delete', 'user-feed.create'])->get();
        $roles = Role::whereIn('name', ['admin', 'user-feed.viewer', 'user-feed.editor'])->get();

        foreach ($roles as $role) {
            $role->detachPermissions($permissions);
            if ($role->editable) {
                $role->delete();
            }
        }

        foreach ($permissions as $permission) {
            $permission->delete();
        }


    }
}
