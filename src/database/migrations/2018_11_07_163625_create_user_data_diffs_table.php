<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDataDiffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccps_userfeed_user_data_diffs', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('old_batch_id')->nullable();
            $table->uuid('new_batch_id');
            $table->json('metrics');
            $table->json('data');
            $table->boolean('encrypted');
            $table->enum('status', ['pending', 'approved', 'expanded', 'held', 'ignored', 'error'])->default('pending');
            $table->timestamps();

            $table->foreign('old_batch_id')->references('id')->on('ccps_userfeed_batches');
            $table->foreign('new_batch_id')->references('id')->on('ccps_userfeed_batches');

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccps_userfeed_user_data_diffs');
    }
}
