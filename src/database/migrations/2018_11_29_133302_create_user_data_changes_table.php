<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDataChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccps_userfeed_user_data_changes', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('user_data_diff_id');
            $table->json('data');
            $table->json('changed_data');
            $table->boolean('encrypted');
            $table->timestamps();

            $table->primary('id');

            $table->foreign('user_data_diff_id')->references('id')->on('ccps_userfeed_user_data_diffs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccps_userfeed_user_data_changes');
    }
}
