<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccps_userfeed_user_data', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('batch_id');
            $table->string('entity_id');
            $table->binary('data');
            $table->string('hash');
            $table->boolean('encrypted');
            $table->timestamps();

            $table->foreign('batch_id')->references('id')->on('ccps_userfeed_batches')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccps_userfeed_user_data');
    }
}
