<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserfeedJsonFieldsToLongtext extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $connection = config('database.default');
        $prefix = config('database.connections.' . $connection . '.prefix');
        \DB::statement('ALTER TABLE ' . $prefix . 'ccps_userfeed_user_data_diffs MODIFY data longtext NULL');
        \DB::statement('ALTER TABLE ' . $prefix . 'ccps_userfeed_user_data_diffs MODIFY metrics longtext NULL');

        Schema::table('ccps_userfeed_user_data_changes', function (Blueprint $table) {
            $table->longText('data')->nullable()->change();
            $table->longText('changed_data')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $connection = config('database.default');
        $prefix = config('database.connections.' . $connection . '.prefix');
        \DB::statement('ALTER TABLE ' . $prefix . 'ccps_userfeed_user_data_diffs MODIFY data json NOT NULL');
        \DB::statement('ALTER TABLE ' . $prefix . 'ccps_userfeed_user_data_diffs MODIFY metrics json NOT NULL');

        Schema::table('ccps_userfeed_user_data_changes', function (Blueprint $table) {
            $table->json('data')->nullable(false)->change();
            $table->json('changed_data')->nullable(false)->change();
        });
    }
}
