<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlarmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccps_userfeed_alarms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('class');
            $table->string('cronjob_class');
            $table->boolean('active')->default(false);
            $table->string('log_channel')->default('stack');
            $table->enum('log_level',
                ['debug', 'info', 'notice', 'warning', 'error', 'critical', 'alert', 'emergency'])->default('info');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccps_userfeed_alarms');
    }
}
