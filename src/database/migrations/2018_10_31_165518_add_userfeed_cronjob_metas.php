<?php

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddUserfeedCronjobMetas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            CronjobMeta::create([
                'class'  => 'Uncgits\Ccps\UserFeed\Cronjobs\FetchNewUserDataBatch',
                'status' => 'disabled'
            ]);

            CronjobMeta::create([
                'class'  => 'Uncgits\Ccps\UserFeed\Cronjobs\ExpandUserDataFromBatch',
                'status' => 'disabled'
            ]);

            CronjobMeta::create([
                'class'  => 'Uncgits\Ccps\UserFeed\Cronjobs\CreateUserDataDiff',
                'status' => 'disabled'
            ]);

            CronjobMeta::create([
                'class'  => 'Uncgits\Ccps\UserFeed\Cronjobs\ExpandUserDataChangesFromDiff',
                'status' => 'disabled'
            ]);

            CronjobMeta::create([
                'class'  => 'Uncgits\Ccps\UserFeed\Cronjobs\PruneOldDiffsAndBatches',
                'status' => 'disabled'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $meta = CronjobMeta::whereIn('class', [
            'Uncgits\Ccps\UserFeed\Cronjobs\FetchNewUserDataBatch',
            'Uncgits\Ccps\UserFeed\Cronjobs\ExpandUserDataFromBatch',
            'Uncgits\Ccps\UserFeed\Cronjobs\CreateUserDataDiff',
            'Uncgits\Ccps\UserFeed\Cronjobs\ExpandUserDataChangesFromDiff',
            'Uncgits\Ccps\UserFeed\Cronjobs\PruneOldDiffsAndBatches',
        ])->delete();
    }
}
