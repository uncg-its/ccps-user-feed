<?php

namespace Uncgits\Ccps\UserFeed\Controllers;

use Illuminate\Http\Request;
use Uncgits\Ccps\UserFeed\Batch;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class BatchController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'user-feed';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    private function acl()
    {
        $this->middleware('permission:user-feed.view')->only(['index', 'show']);
        $this->middleware('permission:user-feed.edit')->only(['update']);
        $this->middleware('permission:user-feed.delete')->only('destroy');
    }

    public function index()
    {
        return view($this->viewPath . 'batches.index')->with([
            'batches' => new CcpsPaginator(Batch::select('id', 'metrics', 'encrypted', 'status', 'created_at')->orderByDesc('created_at')->get())
        ]);
    }

    public function show(Batch $batch, Request $request)
    {
        $userData = $batch->user_data;
        $keys = $userData->isEmpty() ? [] : array_keys($userData->first()->data);

        if ($request->filled('field')) {
            $userData = $userData->filter(function ($data) use ($request) {
                return \Str::contains(strtolower($data->data[$request->field]), strtolower($request->value));
            });
        }

        return view($this->viewPath . 'batches.show')->with([
            'batch'    => $batch,
            'userData' => new CcpsPaginator($userData),
            'keys'     => $keys
        ]);
    }

    public function update(Batch $batch, Request $request)
    {
        $request->validate([
            'status' => 'required|in:approved,ignored,held'
        ]);

        $batch->update(['status' => $request->status]);
        flash('Batch status updated.')->success();
        \Log::channel(config('ccps-user-feed.log_channel'))->info('Batch ' . $batch->id . ' marked \'' . $request->status . '\' by user ' . auth()->user()->email);
        return redirect()->back();
    }

    public function destroy(Batch $batch)
    {
        \DB::beginTransaction();
        try {
            $batch->delete();
            \DB::commit();
            flash('Batch deleted successfully.')->success();
            return redirect()->route('user-feed.batches.index');
        } catch (\Exception $e) {
            \DB::rollBack();
            flash('Error during request: ' . $e->getMessage())->error();
            return redirect()->back();
        }
    }
}
