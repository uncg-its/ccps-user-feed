<?php

namespace Uncgits\Ccps\UserFeed\Controllers;

use Uncgits\Ccps\UserFeed\Batch;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use Uncgits\Ccps\UserFeed\UserDataDiff;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class UserFeedController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'user-feed';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    private function acl()
    {
        $this->middleware('permission:user-feed.view')->only(['index']);
    }

    public function index()
    {
        $heldBatches = new CcpsPaginator(Batch::where('status', 'held')->orderBy('created_at', 'desc')->get());
        $heldDiffs = new CcpsPaginator(UserDataDiff::where('status', 'held')->orderBy('created_at', 'desc')->get());

        $heldDataExists = ($heldBatches->count() > 0) || ($heldDiffs->count() > 0);

        return view($this->viewPath . '.index')->with(compact('heldDataExists'));
    }
}
