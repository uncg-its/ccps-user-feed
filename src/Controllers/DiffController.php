<?php

namespace Uncgits\Ccps\UserFeed\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;
use Uncgits\Ccps\UserFeed\UserDataDiff;

class DiffController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'user-feed';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    private function acl()
    {
        $this->middleware('permission:user-feed.view')->only(['index', 'show']);
        $this->middleware('permission:user-feed.edit')->only(['update']);
        $this->middleware('permission:user-feed.delete')->only('destroy');
    }

    public function index()
    {
        return view($this->viewPath . 'diffs.index')->with([
            'diffs' => new CcpsPaginator(UserDataDiff::select('id', 'old_batch_id', 'new_batch_id', 'metrics', 'encrypted', 'status', 'created_at')->orderByDesc('created_at')->get())
        ]);
    }

    public function show(UserDataDiff $diff, Request $request)
    {
        $changes = $diff->user_data_changes;
        $keys = $changes->isEmpty() ? [] : array_keys($changes->first()->data);

        if ($request->filled('field')) {
            $changes = $changes->filter(function ($data) use ($request) {
                return \Str::contains(strtolower($data->data[$request->field]), strtolower($request->value));
            });
        }

        return view($this->viewPath . 'diffs.show')->with([
            'diff'    => $diff,
            'changes' => new CcpsPaginator($changes),
            'keys'    => $keys
        ]);
    }

    public function update(UserDataDiff $diff, Request $request)
    {
        $request->validate([
            'status' => 'required|in:approved,ignored,held'
        ]);

        $diff->update(['status' => $request->status]);
        flash('Diff status updated.')->success();
        \Log::channel(config('ccps-user-feed.log_channel'))->info('Diff ' . $diff->id . ' marked \'' . $request->status . '\' by user ' . auth()->user()->email);
        return redirect()->back();
    }

    public function destroy(UserDataDiff $diff)
    {
        \DB::beginTransaction();
        try {
            $diff->delete();
            \DB::commit();
            flash('Diff deleted successfully.')->success();
            return redirect()->route('user-feed.diffs.index');
        } catch (\Exception $e) {
            \DB::rollBack();
            flash('Error during request: ' . $e->getMessage())->error();
            return redirect()->back();
        }
    }
}
