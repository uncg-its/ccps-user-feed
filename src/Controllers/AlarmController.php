<?php

namespace Uncgits\Ccps\UserFeed\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Uncgits\Ccps\UserFeed\Alarm;
use App\Http\Controllers\Controller;
use Symfony\Component\Finder\Finder;
use Uncgits\Ccps\Support\CcpsPaginator;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class AlarmController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'user-feed';

    protected $alarmFolder = "";
    protected $alarmClasses = [];
    protected $cronjobClasses = [];

    protected $logChannels = [];
    protected $logLevels = [
        'debug'     => 'debug',
        'info'      => 'info',
        'notice'    => 'notice',
        'warning'   => 'warning',
        'error'     => 'error',
        'critical'  => 'critical',
        'alert'     => 'alert',
        'emergency' => 'emergency'
    ];

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();

        $this->alarmFolder = config('ccps-user-feed.alarms_folder');
    }

    private function resolveClassFolders()
    {
        $this->alarmClasses = $this->getClassArrayFromFolder(base_path($this->alarmFolder));

        $cronjobClassFolder = base_path('vendor/uncgits/ccps-user-feed/src/Cronjobs');
        $this->cronjobClasses = $this->getClassArrayFromFolder($cronjobClassFolder, 'UserFeedCronjob');

        foreach (array_keys(config('logging.channels')) as $channel) {
            $this->logChannels[$channel] = $channel;
        }
    }

    private function acl()
    {
        $this->middleware('permission:user-feed.view')->only(['index', 'show', 'heldData']);
        $this->middleware('permission:user-feed.edit')->only(['edit', 'update', 'manageHeldData']);
        $this->middleware('permission:user-feed.delete')->only('destroy');
        $this->middleware('permission:user-feed.create')->only(['create', 'store']);
    }

    public function getClassArrayFromFolder($folder, $excludes = [])
    {
        if (!is_array($excludes)) {
            $excludes = [$excludes];
        }

        $classes = [
            '' => 'none',
        ];

        if (!\File::exists($folder)) {
            flash('Warning: Folder ' . $folder . ' does not exist!')->warning();
            return $classes;
        }

        $classFinder = new Finder();
        $classFinder->files()->in($folder)->name('*.php');
        foreach ($classFinder as $class) {
            $filename = str_replace('.php', '', $class->getFilename());

            if (!in_array($filename, $excludes)) {
                $classes[$filename] = $filename;
            }
        }

        return $classes;
    }


    public function index()
    {
        $alarms = new CcpsPaginator(Alarm::all());

        return view($this->viewPath . 'alarms.index')->with(compact('alarms'));
    }

    public function show(Alarm $alarm)
    {
        return view($this->viewPath . 'alarms.show')->with(compact('alarm'));
    }

    public function create()
    {
        $this->resolveClassFolders();

        if (count($this->alarmClasses) == 1) {
            // if we have only 'none'
            flash("You must have at least one Alarm class created in folder '{$this->alarmFolder}' before you can proceed.")->warning();
            return redirect()->back();
        }

        return view($this->viewPath . 'alarms.create')->with([
            'alarmClasses'   => $this->alarmClasses,
            'cronjobClasses' => $this->cronjobClasses,
            'logChannels'    => $this->logChannels,
            'logLevels'      => $this->logLevels
        ]);
    }

    public function store(Request $request)
    {
        $this->resolveClassFolders();

        $request->validate([
            'name'          => 'required',
            'class'         => ['required', Rule::in($this->alarmClasses)],
            'cronjob_class' => ['required', Rule::in($this->cronjobClasses)],
            'log_level'     => ['required', Rule::in($this->logLevels)],
            'log_channel'   => ['required', Rule::in($this->logChannels)],
        ]);

        $createArray = $request->only([
            'name',
            'class',
            'cronjob_class',
            'log_level',
            'log_channel'
        ]);

        // massage checkboxes
        $checkboxes = ['active'];
        foreach ($checkboxes as $checkbox) {
            if ($request->has($checkbox)) {
                $createArray[$checkbox] = true;
            }
        }

        \DB::beginTransaction();

        try {
            Alarm::create($createArray);
            \DB::commit();
            flash('Alarm created successfully.')->success();
            return redirect()->route('user-feed.alarms.index');
        } catch (\Exception $e) {
            \DB::rollBack();
            flash('Error during request: ' . $e->getMessage())->error();
            return redirect()->back();
        }
    }

    public function edit(Alarm $alarm)
    {
        $this->resolveClassFolders();

        if (count($this->alarmClasses) == 1) {
            // if we have only 'none'
            flash("You must have at least one Alarm class created in folder '{$this->alarmFolder}' before you can proceed.")->warning();
            return redirect()->back();
        }

        return view($this->viewPath . 'alarms.edit')->with([
            'alarm'          => $alarm,
            'alarmClasses'   => $this->alarmClasses,
            'cronjobClasses' => $this->cronjobClasses,
            'logChannels'    => $this->logChannels,
            'logLevels'      => $this->logLevels
        ]);
    }

    public function update(Request $request, Alarm $alarm)
    {
        $this->resolveClassFolders();

        $request->validate([
            'name'          => 'required',
            'class'         => ['required', Rule::in($this->alarmClasses)],
            'cronjob_class' => ['required', Rule::in($this->cronjobClasses)],
            'log_level'     => ['required', Rule::in($this->logLevels)],
            'log_channel'   => ['required', Rule::in($this->logChannels)],
        ]);

        $updateArray = $request->only([
            'name',
            'class',
            'cronjob_class',
            'log_level',
            'log_channel'
        ]);

        // massage checkboxes
        $checkboxes = ['active'];
        foreach ($checkboxes as $checkbox) {
            $updateArray[$checkbox] = $request->has($checkbox);
        }

        \DB::beginTransaction();

        try {
            $alarm->update($updateArray);
            \DB::commit();
            flash('Alarm edited successfully.')->success();
            return redirect()->route('user-feed.alarms.index');
        } catch (\Exception $e) {
            \DB::rollBack();
            flash('Error during request: ' . $e->getMessage())->error();
            return redirect()->back();
        }
    }

    public function destroy(Alarm $alarm)
    {
        \DB::beginTransaction();
        try {
            $alarm->delete();
            \DB::commit();
            flash('Alarm deleted successfully.')->success();
            return redirect()->route('user-feed.alarms.index');
        } catch (\Exception $e) {
            \DB::rollBack();
            flash('Error during request: ' . $e->getMessage())->error();
            return redirect()->back();
        }
    }
}
