<?php

namespace Uncgits\Ccps\UserFeed\Controllers;

use Uncgits\Ccps\UserFeed\Batch;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use Uncgits\Ccps\UserFeed\UserDataDiff;
use Uncgits\Ccps\Traits\ResolvesViewPathFromModuleConfig;

class HeldDataController extends Controller
{
    use ResolvesViewPathFromModuleConfig;

    protected $moduleName = 'user-feed';

    public function __construct()
    {
        $this->acl();
        $this->resolveViewPath();
    }

    private function acl()
    {
        $this->middleware('permission:user-feed.view')->only(['index']);
    }


    public function index()
    {
        $heldBatches = new CcpsPaginator(Batch::where('status', 'held')->orderBy('created_at', 'desc')->get());
        $heldDiffs = new CcpsPaginator(UserDataDiff::where('status', 'held')->orderBy('created_at', 'desc')->get());

        return view($this->viewPath . 'held-data.index')->with(compact(['heldBatches', 'heldDiffs']));
    }

    private function changeDiffStatus(UserDataDiff $diff, $newStatus)
    {
        $methodMap = [
            'approved' => 'approve',
            'ignored'  => 'ignore',
            'held'     => 'hold',
        ];

        $method = $methodMap[$newStatus];

        $diff->$method();
        flash('Diff status updated.')->success();
        \Log::channel(config('ccps-user-feed.log_channel'))->info('Diff ' . $diff->id . ' marked \'' . $newStatus . '\' by user ' . auth()->user()->email);
        return redirect()->route('user-feed.held-data.diff.show', ['diff' => $diff->id]);
    }

    public function approveDiff(UserDataDiff $diff)
    {
        return $this->changeDiffStatus($diff, 'approved');
    }

    public function ignoreDiff(UserDataDiff $diff)
    {
        return $this->changeDiffStatus($diff, 'ignored');
    }

    public function holdDiff(UserDataDiff $diff)
    {
        return $this->changeDiffStatus($diff, 'held');
    }

    public function deleteDiff(UserDataDiff $diff)
    {
        if ($diff->delete()) {
            flash('Diff ' . $diff->id . ' deleted successfully.')->success();
            return redirect()->route('user-feed.held-data.index');
        } else {
            flash('Error deleting Diff ' . $diff->id . '.')->error();
            return redirect()->back();
        }
    }
}
