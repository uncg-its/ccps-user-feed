<?php

namespace Uncgits\Ccps\UserFeed\Commands;

use Illuminate\Console\Command;

class MakeAlarm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccps:make:alarm {name : The name of the Alarm class to be created}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an Alarm for the CCPS User Feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $name = studly_case($this->argument('name'));
            $alarmFolder = config('ccps-user-feed.alarms_folder');
            $alarmPath = base_path($alarmFolder);

            // check to see if file already exists
            $destinationFile = $alarmPath . $name . '.php';
            if (file_exists($destinationFile)) {
                throw new \Exception("Alarm File '$destinationFile' already exists.");
            }

            // stub information
            $file = 'vendor/uncgits/ccps-user-feed/src/stubs/Alarm.stub';
            $stub = file_get_contents(base_path($file));

            $namespaceBits = array_map(function ($value) {
                return ucwords($value);
            }, explode('/', $alarmFolder));

            $namespace = implode('\\', array_filter($namespaceBits, function ($value) {
                return $value != '';
            }));

            $namespaceString = "NAMESPACE";
            // replace stub data
            $stub = str_replace($namespaceString, $namespace, $stub);

            $classString = "CLASSNAME";
            // replace stub data
            $stub = str_replace($classString, $name, $stub);

            // create directory if it does not exist
            if (!is_dir($alarmPath)) {
                mkdir($alarmPath);
                $this->info("Created 'alarms' folder.");
            }

            // create Alarm from stub
            if (file_put_contents($destinationFile, $stub)) {
                $this->info("New alarm script file '$destinationFile' generated successfully.");
                return true;
            } else {
                throw new \Exception('Could not create Alarm file. Check permissions and try again.');
            }
        } catch (\Exception $e) {
            $this->error('Error: ' . $e->getMessage());
            return false;
        }
    }
}
