<?php

namespace Uncgits\Ccps\UserFeed;

use Illuminate\Database\Eloquent\Model;

class Alarm extends Model
{
    protected $table = 'ccps_userfeed_alarms';

    protected $guarded = [];

    public function user_data()
    {
        return $this->morphedByMany(UserData::class, 'alarmable');
    }

    public function user_data_diffs()
    {
        return $this->morphedByMany(UserDataDiff::class, 'alarmable');
    }
}
