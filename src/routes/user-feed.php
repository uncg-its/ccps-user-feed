<?php

/*

Copy example below as a template, replace info in CAPS
This will be pulled in automatically via CCPS module definitions

Route::group(['prefix' => 'NAME'], function() {
    Route::get('/', 'MYCONTROLLER@METHOD')->name('ROUTENAME');
    ... other routes ...
});

*/


Route::name('user-feed.')->prefix('user-feed')->namespace('\Uncgits\Ccps\UserFeed\Controllers')->group(function () {
    Route::get('/', 'UserFeedController@index')->name('index');
    Route::resource('alarms', 'AlarmController');
    Route::get('held-data', 'HeldDataController@index')->name('held-data.index');

    Route::resource('batches', 'BatchController')->except('edit');
    Route::resource('diffs', 'DiffController')->except('edit');
});
