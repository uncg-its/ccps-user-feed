<?php

return [

    'alarms_folder' => 'app/Alarms/', // include trailing slash

    'encrypt_user_data' => env('ENCRYPT_USER_DATA', true),

    'diffs_to_keep' => env('DIFFS_TO_KEEP', 1),

    'grouper' => [
        'entity_id_field'  => env('GROUPER_ENTITY_ID_FIELD', 'uidnumber'),
        'source_groups'    => explode(',', env('GROUPER_SOURCE_GROUPS', '')),
        'user_feed_fields' => explode(
            ',',
            env('GROUPER_USER_FEED_FIELDS', env('GROUPER_API_SUBJECT_ATTRIBUTE_NAMES', 'description,name'))
        ),
    ],

    'dropped_fields'           => explode(',', env('USER_FEED_DROPPED_FIELDS', '')),
    'ignored_fields'           => explode(',', env('USER_FEED_IGNORED_FIELDS', '')),
    'required_nonblank_fields' => explode(',', env('USER_FEED_REQUIRED_NONBLANK_FIELDS', '')),
    'fetch_tries_per_group'    => env('USER_FEED_FETCH_TRIES', 3),

    'log_channel' => env('USER_FEED_LOG_CHANNEL', 'stack'),

    'php' => [
        'temp_memory_limit'       => env('USER_FEED_MEMORY_LIMIT', null),
        'temp_max_execution_time' => env('USER_FEED_MAX_EXECUTION_TIME', null),
    ]

];
