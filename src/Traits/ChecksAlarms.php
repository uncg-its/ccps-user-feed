<?php

namespace Uncgits\Ccps\UserFeed\Traits;

use Uncgits\Ccps\UserFeed\Alarm;
use Uncgits\Ccps\UserFeed\Events\UserFeedAlarmCheckFailed;
use Uncgits\Ccps\UserFeed\Events\UserFeedAlarmCheckPassed;

trait ChecksAlarms
{
    private function checkAlarms($data)
    {
        $thisClass = (new \ReflectionClass($this))->getShortName();
        $alarmsToRun = Alarm::where('cronjob_class', $thisClass)->where('active', true)->get();

        \Log::channel(config('ccps-user-feed.log_channel'))
            ->debug('Found ' . $alarmsToRun->count() . ' alarms to run.');

        $passesAll = true;

        foreach ($alarmsToRun as $alarmData) {
            try {
                $namespace = str_replace('/', '\\', ucwords(config('ccps-user-feed.alarms_folder')));
                $alarmClass = '\\' . $namespace . $alarmData->class;

                // instantiate and execute with the model
                $alarm = new $alarmClass();
                $passes = $alarm->passes($data);

                if ($passes) {
                    // broadcast failure event
                    event(new UserFeedAlarmCheckPassed($alarmClass, $data, __CLASS__));
                    // take any extra steps coded into the Alarm itself
                    $alarm->onPass($data);

                    \Log::channel(config('ccps-user-feed.log_channel'))
                        ->debug('Alarm ' . $alarmClass . ' check passed.');
                } else {
                    // broadcast failure event
                    event(new UserFeedAlarmCheckFailed($alarmClass, $data, __CLASS__));
                    // take any extra steps coded into the Alarm itself
                    $alarm->onFail($data);

                    $passesAll = false;

                    // allow app-level notifications to be in charge of how this goes.
                    $level = $alarmData->log_level;
                    \Log::channel($alarmData->log_channel)->$level('Cronjob ' . $thisClass . ' failed check in Alarm class ' . $alarmClass);
                }
            } catch (\Exception $e) {
                throw $e;
            }
        }

        return $passesAll;
    }
}
