<?php

namespace Uncgits\Ccps\UserFeed\Traits;

trait HasStatusClassAndIcon
{
    private $statuses = [
        'pending'  => [
            'classes' => 'text-blue-500',
            'icon'    => 'fas fa-clock',
        ],
        'approved' => [
            'classes' => 'text-green-500',
            'icon'    => 'fas fa-thumbs-up',
        ],
        'expanded' => [
            'classes' => 'text-green-500',
            'icon'    => 'fas fa-box-open',
        ],
        'diffed'   => [
            'classes' => 'text-green-500',
            'icon'    => 'fas fa-check',
        ],
        'held'     => [
            'classes' => 'text-red-500',
            'icon'    => 'fas fa-hand-paper',
        ],
        'ignored'  => [
            'classes' => 'text-yellow-500',
            'icon'    => 'fas fa-archive',
        ],
        'error'    => [
            'classes' => 'text-red-500',
            'icon'    => 'fas fa-exclamation-triangle'
        ]
    ];

    public function getStatusClassAttribute()
    {
        return $this->statuses[$this->status]['classes'];
    }

    public function getStatusIconAttribute()
    {
        return $this->statuses[$this->status]['icon'];
    }
}
