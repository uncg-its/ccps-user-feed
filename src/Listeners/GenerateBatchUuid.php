<?php

namespace Uncgits\Ccps\UserFeed\Listeners;

use Uncgits\Ccps\UserFeed\Events\BatchCreating;

class GenerateBatchUuid
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return void
     */
    public function handle(BatchCreating $event)
    {
        $event->batch->setUuid();
    }
}
