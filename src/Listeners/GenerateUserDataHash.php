<?php

namespace Uncgits\Ccps\UserFeed\Listeners;

use Uncgits\Ccps\UserFeed\Events\UserDataSaving;

class GenerateUserDataHash
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return void
     */
    public function handle(UserDataSaving $event)
    {
        $event->userData->setHash();
    }
}
