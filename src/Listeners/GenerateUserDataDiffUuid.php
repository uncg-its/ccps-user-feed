<?php

namespace Uncgits\Ccps\UserFeed\Listeners;

use Uncgits\Ccps\UserFeed\Events\UserDataDiffCreating;

class GenerateUserDataDiffUuid
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return void
     */
    public function handle(UserDataDiffCreating $event)
    {
        $event->diff->setUuid();
    }
}
