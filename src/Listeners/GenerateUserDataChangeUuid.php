<?php

namespace Uncgits\Ccps\UserFeed\Listeners;

use Uncgits\Ccps\UserFeed\Events\UserDataChangeCreating;

class GenerateUserDataChangeUuid
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return void
     */
    public function handle(UserDataChangeCreating $event)
    {
        $event->change->setUuid();
    }
}
