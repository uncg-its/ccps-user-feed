<?php

namespace Uncgits\Ccps\UserFeed;

use Uncgits\Ccps\UserFeed\Events\BatchCreating;
use Uncgits\Ccps\UserFeed\Events\UserDataSaving;
use Uncgits\Ccps\UserFeed\Events\UserDataDiffCreating;
use Uncgits\Ccps\UserFeed\Listeners\GenerateBatchUuid;
use Uncgits\Ccps\UserFeed\Events\UserDataChangeCreating;
use Uncgits\Ccps\UserFeed\Listeners\GenerateUserDataHash;
use Uncgits\Ccps\UserFeed\Listeners\GenerateUserDataDiffUuid;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Uncgits\Ccps\UserFeed\Listeners\GenerateUserDataChangeUuid;

class UserFeedServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // register Artisan commands, define publishing, define route-loading or migration-loading... etc.
        if ($this->app->runningInConsole()) {
            $this->commands([
                \Uncgits\Ccps\UserFeed\Commands\MakeAlarm::class,
            ]);
        }

        // views
        $this->loadViewsFrom(
            __DIR__ . '/views',
            'ccps-user-feed'
        ); // reference in app with view('MODULENAME::viewname')

        // migrations
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations/');

        // factories
        $this->app->make('Illuminate\Database\Eloquent\Factory')->load(__DIR__ . '/database/factories');

        // publishing
        $this->publishes([
            __DIR__ . '/config.php' => config_path('ccps-user-feed.php'),
        ]);

        // bind hash generation to any attempt to save a record to the database
        \Event::listen(
            UserDataSaving::class,
            GenerateUserDataHash::class
        );

        // bind listener for making uuids on batches
        \Event::listen(
            BatchCreating::class,
            GenerateBatchUuid::class
        );

        // bind listener for making uuids on user data diffs
        \Event::listen(
            UserDataDiffCreating::class,
            GenerateUserDataDiffUuid::class
        );

        // bind listener for making uuids on user data changes
        \Event::listen(
            UserDataChangeCreating::class,
            GenerateUserDataChangeUuid::class
        );
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // merge in config if user opts not to publish.
        $this->mergeConfigFrom(
            __DIR__ . '/config.php',
            'ccps-user-feed'
        );
    }
}
