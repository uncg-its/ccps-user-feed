@extends('layouts.wrapper', [
    'pageTitle' => 'User Feed - Held Data'
])

@section('content')
    {!! Breadcrumbs::render('user-feed.held-data') !!}
    <x-h1>Held Data</x-h1>
    <x-p>This section allows you to manage any held data (Batches or Diffs) that triggered an alarm during processing.</x-p>
    <x-hr />
    @if($heldBatches->isNotEmpty())
    <x-h2>Held Batches</x-h2>
        <x-table>
            <x-slot name="th">
                <x-th scope="col">@sortablelink('id')</x-th>
                <x-th scope="col">@sortablelink('created_at')</x-th>
                <x-th scope="col">Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach($heldBatches as $batch)
                    <x-tr>
                        <x-td>
                            <x-a href="{{ route('user-feed.batches.show', $batch) }}">{{ $batch->id }}</x-a>
                        </x-td>
                        <x-td>{{ $batch->created_at }}</x-td>
                        <x-td>
                            <x-button color="blue" size="sm" href="{{ route('user-feed.batches.show', ['batch' => $batch->id]) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
    @endif

    @if($heldDiffs->isNotEmpty())
    <x-h2>Held Diffs</x-h2>
        <x-table>
            <x-slot name="th">
                <x-th scope="col">@sortablelink('id')</x-th>
                <x-th scope="col">@sortablelink('created_at')</x-th>
                <x-th scope="col">Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach($heldDiffs as $diff)
                    <x-tr>
                        <x-td>
                            <x-a href="{{ route('user-feed.diffs.show', $diff) }}">{{ $diff->id }}</x-a>
                        </x-td>
                        <x-td>{{ $diff->created_at }}</x-td>
                        <x-td>
                            <x-button color="blue" size="sm" href="{{ route('user-feed.diffs.show', ['diff' => $diff->id]) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
    @endif

    @if($heldBatches->isEmpty() && $heldDiffs->isEmpty())
        <x-p>No held data exists.</x-p>
    @endif
@endsection
