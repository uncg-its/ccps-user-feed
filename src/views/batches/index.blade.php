@extends('layouts.wrapper', [
    'pageTitle' => 'User Feed Batches | Index'
])

@section('content')
    {!! Breadcrumbs::render('user-feed.batches') !!}
    <x-h1>Batches</x-h1>
    <x-p>Shows all current (un-pruned) Batches fetched by the User Feed for this application</x-p>
    <x-hr />
    @if ($batches->isNotEmpty())
        <x-table>
            <x-slot name="th">
                <x-th>ID</x-th>
                <x-th>Record Count</x-th>
                <x-th>Encrypted?</x-th>
                <x-th>Status</x-th>
                <x-th>Created At</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($batches as $batch)
                    <x-tr>
                        <x-td>{{ $batch->id }}</x-td>
                        <x-td>{{ $batch->metrics['source_records_count'] }}</x-td>
                        <x-td>{{ $batch->encrypted ? 'yes' : 'no' }}</x-td>
                        <x-td><span class="{{ $batch->statusClass }}"><i class="{{ $batch->statusIcon }}"></i> {{ $batch->status }}</span></x-td>
                        <x-td>{{ $batch->created_at }}</x-td>
                        <x-td>
                            <x-button color="blue" href="{{ route('user-feed.batches.show', $batch) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
        {!! $batches->links() !!}
    @else
        <x-p>No Batches to show.</x-p>
    @endif
@endsection
