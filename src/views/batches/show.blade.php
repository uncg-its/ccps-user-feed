@extends('layouts.wrapper', [
    'pageTitle' => 'User Feed Batches | Batch ' . $batch->id
])

@section('content')
    {!! Breadcrumbs::render('user-feed.batches.show', $batch) !!}
    <x-h1>Batch {{ $batch->id }} ({{ $batch->created_at }})</x-h1>
    <x-p>Details on a Batch, including its User Data entries</x-p>
    <x-hr />
    <div class="grid gap-4 grid-cols-2">
        <x-card title="Batch Info">
            <x-p><strong>Encrypted</strong>: {{ $batch->encrypted ? 'yes' : 'no' }}</x-p>
            <x-p><strong>Created at</strong>: {{ $batch->created_at }}</x-p>
            <x-p><strong>Status</strong>:
                <span class="{{ $batch->statusClass }}"><i class="{{ $batch->statusIcon }}"></i> {{ $batch->status }}</span>
            </x-p>
            <x-p>
                <strong>Diff as Old Batch</strong>:
                @if (is_null($batch->diff_as_old_batch))
                    none
                @else
                    <x-a href="{{ route('user-feed.diffs.show', $batch->diff_as_old_batch) }}">{{ $batch->diff_as_old_batch->id }}</x-a>
                @endif
            </x-p>
            <x-p>
                <strong>Diff as New Batch</strong>:
                @if (is_null($batch->diff_as_new_batch))
                    none
                @else
                    <x-a href="{{ route('user-feed.diffs.show', $batch->diff_as_new_batch) }}">{{ $batch->diff_as_new_batch->id }}</x-a>
                @endif
            </x-p>

            <x-p><strong>Metrics</strong>: </x-p>
            <pre>{{ json_encode($batch->metrics, JSON_PRETTY_PRINT) }}</pre>
        </x-card>
        <x-card title="Actions">
            @if($batch->status == 'held')
                <div class="flex">

                    <x-form-patch action="{{ route('user-feed.batches.update', ['batch' => $batch->id, 'status' => 'approved']) }}">
                        <x-button color="green" size="sm">
                            <x-fas>thumbs-up</x-fas> Approve
                        </x-button>
                    </x-form-patch>

                    <x-form-patch action="{{ route('user-feed.batches.update', ['batch' => $batch->id, 'status' => 'ignored']) }}">
                        <x-button color="yellow" size="sm">
                            <x-fas>archive</x-fas> Ignore
                        </x-button>
                    </x-form-patch>

                    <x-form-delete action="{{ route('user-feed.batches.destroy', ['batch' => $batch->id]) }}" confirm="Are you sure? This cannot be undone!">
                        <x-button color="red" size="sm">
                            <x-fas>trash</x-fas> Delete
                        </x-button>
                    </x-form-delete>
                </div>

                <x-p class="mt-2">
                    <small class="text-muted">Approving a batch allows it to be used in diffs. Ignoring a batch
                        will remove it from consideration in diffs. Deleting a batch is a last resort and should
                        be used with caution.
                    </small>
                </x-p>
            @elseif($batch->status == 'approved')
                <div class="flex">
                    <x-form-patch action="{{ route('user-feed.batches.update', ['batch' => $batch->id, 'status' => 'held']) }}">
                        <x-button color="red" size="sm">
                            <x-fas>hand-paper</x-fas> Hold
                        </x-button>
                    </x-form-patch>
                </div>

                <x-p class="mt-2">
                    <small class="text-muted">
                        Holding a batch allows it to be checked before being expanded into User Data. You
                        should consider also stopping appropriate cronjobs manually if holding a Batch manually.
                    </small>
                </x-p>
            @else
                <x-p>No actions available.</x-p>
            @endif
        </x-card>
    </div>

    @if($userData->isNotEmpty())
        <x-card title="Search / Filter">
            <x-form-get action="{{ route('user-feed.batches.show', ['batch' => $batch]) }}">
                <div class="flex gap-3 items-end">
                    <div class="w-1/3">
                        <x-input-select
                            name="field"
                            required
                            :options="array_combine($keys, $keys)"
                            :selected="request()->get('field', '')"
                        />
                    </div>
                    <div class="w-1/3">
                        <x-input-text
                            name="value"
                            required
                            :value="request()->get('value', '')"
                        />
                    </div>
                    <div class="w-1/3 flex mb-4">
                        <x-button color="green" size="sm">
                            <x-fas>check</x-fas> Submit
                        </x-button>
                        <x-button color="red" size="sm" href="{{ route('user-feed.batches.show', ['batch' => $batch]) }}">
                            <x-fas>undo</x-fas> Reset
                        </x-button>
                    </div>
                </div>
            </x-form-get>
        </x-card>
    @endif

    @if (request()->anyFilled('field', 'value'))
        <div class="w-full">
            <x-alert-info :dismissable="false">Filtering on</x-alert-info>
        </div>
    @endif

    @if ($userData->isNotEmpty())
        <x-table>
            <x-slot name="th">
                <x-th>Entity ID</x-th>
                @foreach ($keys as $key)
                    <x-th>{{ $key }}</x-th>
                @endforeach
            </x-slot>
            <x-slot name="tbody">
                @foreach ($userData as $userDatum)
                    <x-tr>
                        <x-td>{{ $userDatum->entity_id }}</x-td>
                        @foreach ($keys as $key)
                            <x-td>{{ $userDatum->data[$key] }}</x-td>
                        @endforeach
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
    @else
        @if ($batch->status === 'expanded' || $batch->status === 'diffed')
            <x-p>No User Data to show.</x-p>
        @else
            <x-p>This Batch has not been expanded yet.</x-p>
        @endif
    @endif

@endsection
