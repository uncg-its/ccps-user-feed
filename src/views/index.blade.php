@extends('layouts.wrapper', [
    'pageTitle' => 'User Feed - Index'
])

@section('content')
    {!! Breadcrumbs::render('user-feed') !!}

    <x-h1>User Feed</x-h1>

    @if($heldDataExists)
        <x-alert-warning>
            WARNING: Held Data exists! Select 'Manage Held Data' below to address.
        </x-alert-warning>
    @endif

    <div class="flex">
        @permission('user-feed.*')
            @include('components.panel-nav', [
                'url' => route('user-feed.batches.index'),
                'fa' => 'fas fa-layer-group',
                'title' => 'Batches'
            ])

            @include('components.panel-nav', [
                'url' => route('user-feed.diffs.index'),
                'fa' => 'fas fa-exchange-alt',
                'title' => 'Diffs'
            ])

            @include('components.panel-nav', [
                'url' => route('user-feed.alarms.index'),
                'fa' => 'fas fa-exclamation-triangle',
                'title' => 'Alarms'
            ])

            @include('components.panel-nav', [
                'url' => route('user-feed.held-data.index'),
                'fa' => 'fas fa-wrench',
                'title' => 'Manage Held Data'
            ])

            @include('components.panel-nav', [
                'url' => route('cronjobs', ['tags' => 'ccps-user-feed']),
                'fa' => 'fas fa-hourglass-half',
                'title' => 'User Feed Cron Jobs'
            ])
        @endpermission
    </div>
@endsection
