<x-card title="Alarm Information">
    <div class="grid gap-4 grid-cols-3">
        <x-input-text
            name="name"
            required
            value="{{ old('name', optional($alarm)->name) }}"
        />
        <x-input-select
            name="class"
            label="Alarm Class"
            :options="$alarmClasses"
            required
            help="Loading from folder: {{ config('ccps-user-feed.alarms_folder') }}"
            value="{{ old('class', optional($alarm)->class) }}"
        />
        <x-input-select
            name="cronjob_class"
            label="Cronjob Class"
            :options="$cronjobClasses"
            help="Alarm will run after this cronjob completes successfully."
            required
            :selected="old('cronjob_class', optional($alarm)->cronjob_class)"
        />
        <x-input-select
            name="log_level"
            label="Log Level"
            :options="$logLevels"
            :selected="old('log_level', optional($alarm)->log_level)"
            help="The level of the log entry if this alarm fails"
        />
        <x-input-select
            name="log_channel"
            label="Log Channel"
            :options="$logChannels"
            :selected="old('log_channel', optional($alarm)->log_channel)"
            help="The channel of the log entry if this alarm fails"
        />
        <div class="flex items-center">
            <x-input-checkbox
                name="active"
                label="Active?"
                :checked="old('active', optional($alarm)->active)"
            />
        </div>
    </div>
    <div class="flex">
        <x-button color="green" size="sm">
            <x-fas>check</x-fas> Submit
        </x-button>
    </div>
</x-card>
