@extends('layouts.wrapper', [
    'pageTitle' => 'Alarms - index'
])

@section('content')
    {!! Breadcrumbs::render('user-feed.alarms') !!}
    <div class="flex items-center justify-between">
        <x-h1>User Feed Alarms</x-h1>
        @permission('user-feed.create')
            <x-button color="green" size="sm" href="{{ route('user-feed.alarms.create') }}">
                <x-fas>plus</x-fas> Add New
            </x-button>
        @endpermission
    </div>
    @if(count($alarms) > 0)
        <x-table>
            <x-slot name="th">
                <x-th scope="col">@sortablelink('name')</x-th>
                <x-th scope="col">@sortablelink('class')</x-th>
                <x-th scope="col">@sortablelink('cronjob_class', 'Cronjob')</x-th>
                <x-th scope="col">@sortablelink('log_level', 'Log Level')</x-th>
                <x-th scope="col">@sortablelink('log_channel', 'Log Channel')</x-th>
                <x-th scope="col">@sortablelink('active', 'Active?')</x-th>
                <x-th scope="col">Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach($alarms as $alarm)
                    <x-tr>
                        <x-td>{{ $alarm->name }}</x-td>
                        <x-td>{{ $alarm->class }}</x-td>
                        <x-td>{{ $alarm->cronjob_class }}</x-td>
                        <x-td>{{ $alarm->log_level }}</x-td>
                        <x-td>{{ $alarm->log_channel }}</x-td>
                        <x-td>{{ $alarm->active ? 'y' : 'n' }}</x-td>
                        <x-td class="flex items-center flex-col">
                            <x-button color="blue" size="sm" href="{{ route('user-feed.alarms.show', ['alarm' => $alarm->id]) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
                            @permission('user-feed.edit')
                                <x-button color="yellow" size="sm" href="{{ route('user-feed.alarms.edit', ['alarm' => $alarm->id]) }}">
                                    <x-fas>edit</x-fas> Edit
                                </x-button>
                            @endpermission

                            @permission('user-feed.delete')
                                <x-form-delete
                                    action="{{ route('user-feed.alarms.destroy', ['alarm' => $alarm->id]) }}"
                                    confirm="Do you really want to delete this alarm?"
                                >
                                    <x-button color="red" size="sm">
                                        <x-fas>trash</x-fas> Delete
                                    </x-button>
                                </x-form-delete>
                            @endpermission
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
        <div class="flex justify-center">
            {!! $alarms->appends(Request::except('page'))->render() !!}
        </div>
    @else
        <x-p>No alarms exist.</x-p>
    @endif
@endsection
