@extends('layouts.wrapper', [
    'pageTitle' => 'Users - Add New'
])

@section('content')
    {!! Breadcrumbs::render('user-feed.alarms.create') !!}

    <x-h1>Add New Alarm</x-h1>

    <x-form-post action="{{ route('user-feed.alarms.store') }}">
        @include('ccps-user-feed::alarms.form', ['alarm' => new stdClass])
    </x-form-post>

@endsection
