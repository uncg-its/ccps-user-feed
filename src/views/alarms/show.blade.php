@extends('layouts.wrapper', [
    'pageTitle' => 'Alarms - ' . $alarm->name
])

@section('content')
    {!! Breadcrumbs::render('user-feed.alarms.show', $alarm) !!}

    <x-h1>Alarm - {{ $alarm->name }}</x-h1>

    <x-card title="Alarm Information">
        <x-p><strong>Name</strong>: {{ $alarm->name }}</x-p>
        <x-p><strong>Class</strong>: {{ $alarm->class }}</x-p>
        <x-p><strong>Active?</strong>: {{ $alarm->active ? 'Yes' : 'No' }}</x-p>
        <x-p><strong>Stops Processing?</strong>: {{ $alarm->active ? 'Yes' : 'No' }}</x-p>
    </x-card>

    <x-card title="Logging">
        <x-p><strong>Channel</strong>: {{ $alarm->log_channel }}</x-p>
        <x-p><strong>Level</strong>: {{ $alarm->log_level }}</x-p>
    </x-card>

    <x-card title="Actions">
        <div class="flex">
            @permission('user-feed.edit')
                <x-button color="yellow" size="sm" href="{{ route('user-feed.alarms.edit', ['alarm' => $alarm->id]) }}">
                    <x-fas>edit</x-fas> Edit
                </x-button>
            @endpermission
            @permission('user-feed.delete')
                <x-form-delete
                    action="{{ route('user-feed.alarms.destroy', ['alarm' => $alarm->id]) }}"
                    confirm="Do you really want to delete this alarm?"
                >
                    <x-button color="red" size="sm">
                        <x-fas>trash</x-fas> Delete
                    </x-button>
                </x-form-delete>
            @endpermission
        </div>
    </x-card>
@endsection
