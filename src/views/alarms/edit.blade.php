@extends('layouts.wrapper', [
    'pageTitle' => 'Alarms - Edit'
])

@section('content')
    {!! Breadcrumbs::render('user-feed.alarms.edit', $alarm) !!}

    <x-h1>Edit Alarm - {{ $alarm->name }}</x-h1>

    <x-form-patch action="{{ route('user-feed.alarms.update', ['alarm' => $alarm->id]) }}">
        @include('ccps-user-feed::alarms.form', ['alarm' => $alarm])
    </x-form-patch>
@endsection
