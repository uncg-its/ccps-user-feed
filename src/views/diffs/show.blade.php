@extends('layouts.wrapper', [
    'pageTitle' => 'User Feed Diffs | Diff ' . $diff->id
])

@section('content')
    {!! Breadcrumbs::render('user-feed.diffs.show', $diff) !!}
    <x-h1>Diff {{ $diff->id }} ({{ $diff->created_at }})</x-h1>
    <x-p>Details on a Diff, including its User Data Change entries</x-p>
    <x-hr />
    <div class="grid gap-4 grid-cols-2">
        <x-card title="Diff Info">
            <x-p><strong>Created at</strong>: {{ $diff->created_at }}</x-p>
            <x-p><strong>Status</strong>:
                <span class="{{ $diff->statusClass }}"><i class="{{ $diff->statusIcon }}"></i> {{ $diff->status }}</span>
            </x-p>

            <x-p><strong>Old Batch</strong>:
                @if (!is_null($diff->old_batch_id))
                    <x-a href="{{ route('user-feed.batches.show', $diff->old_batch_id) }}">{{ $diff->old_batch_id }}</x-a>
                @endif
            </x-p>
            <x-p><strong>New Batch</strong>: <x-a href="{{ route('user-feed.batches.show', $diff->new_batch_id) }}">{{ $diff->new_batch_id }}</x-a></x-p>
            <x-p><strong>Encrypted</strong>: {{ $diff->encrypted ? 'yes' : 'no' }}</x-p>

            <x-p><strong>Metrics</strong>: </x-p>
            <pre>{{ json_encode($diff->metrics, JSON_PRETTY_PRINT) }}</pre>
        </x-card>
        <x-card title="Actions">
            @if($diff->status == 'held')
                <div class="flex">
                    <x-form-patch action="{{ route('user-feed.diffs.update', ['diff' => $diff->id, 'status' => 'approved']) }}">
                        <x-button color="green" size="sm">
                            <x-fas>thumbs-up</x-fas> Approve
                        </x-button>
                    </x-form-patch>

                    <x-form-patch action="{{ route('user-feed.diffs.update', ['diff' => $diff->id, 'status' => 'ignored']) }}">
                        <x-button color="yellow" size="sm">
                            <x-fas>archive</x-fas> Ignore
                        </x-button>
                    </x-form-patch>

                    <x-form-delete action="{{ route('user-feed.diffs.destroy', ['diff' => $diff->id]) }}" confirm="Are you sure? This cannot be undone!">
                        <x-button color="red" size="sm">
                            <x-fas>trash</x-fas> Delete
                        </x-button>
                    </x-form-delete>
                </div>

                <x-p class="mt-2">
                    <small class="text-muted">Approving a diff allows it to proceed in generating user data
                        changes. Ignoring a diff will prevent it from generating user data changes. Deleting a
                        diff is generally a last resort and should be used with caution.
                    </small>
                </x-p>
            @elseif($diff->status == 'approved' || $diff->status == 'ignored')
                <x-form-patch action="{{ route('user-feed.diffs.update', ['diff' => $diff->id, 'status' => 'held']) }}">
                    <x-button color="red" size="sm">
                        <x-fas>hand-paper</x-fas> Hold
                    </x-button>
                </x-form-patch>

                <x-p class="mt-2">
                    <small class="text-muted">
                        Holding a diff allows it to be checked before being expanded into User Data Changes. You
                        should consider also stopping appropriate cronjobs manually if holding a Diff manually.
                    </small>
                </x-p>
            @else
                <x-p>No actions available.</x-p>
            @endif
        </x-card>
    </div>

    @if($changes->isNotEmpty())
        <x-card title="Search / Filter">
            <x-form-get action="{{ route('user-feed.diffs.show', ['diff' => $diff]) }}">
                <div class="flex gap-3 items-end">
                    <div class="w-1/3">
                        <x-input-select
                            name="field"
                            required
                            :options="array_combine($keys, $keys)"
                            :selected="request()->get('field', '')"
                        />
                    </div>
                    <div class="w-1/3">
                        <x-input-text
                            name="value"
                            required
                            :value="request()->get('value', '')"
                        />
                    </div>
                    <div class="w-1/3 flex mb-4">
                        <x-button color="green" size="sm">
                            <x-fas>check</x-fas> Submit
                        </x-button>
                        <x-button color="red" size="sm" href="{{ route('user-feed.diffs.show', ['diff' => $diff]) }}">
                            <x-fas>undo</x-fas> Reset
                        </x-button>
                    </div>
                </div>
            </x-form-get>
        </x-card>
    @endif

    @if (request()->anyFilled('field', 'value'))
        <div class="w-full">
            <x-alert-info :dismissable="false">Filtering on</x-alert-info>
        </div>
    @endif
    @if ($changes->isNotEmpty())
    <div x-data="{show: 'original'}">

        <div class="flex justify-end items-center mb-3">
            <x-button color="blue" size="sm" x-on:click="show='original'" x-show="show=='changed'">
                <x-fas>toggle-on</x-fas> Show Original Data
            </x-button>
            <x-button color="red" size="sm" x-on:click="show='changed'" x-show="show=='original'">
                <x-fas>toggle-off</x-fas> Show Changed Data
            </x-button>
        </div>

        <x-table>
            <x-slot name="th">
                @foreach ($keys as $key)
                    <x-th>{{ $key }}</x-th>
                @endforeach
            </x-slot>
            <x-slot name="tbody">
                @foreach ($changes as $change)
                    <x-tr>
                        @foreach ($keys as $key)
                        <x-td>
                            @if (isset($change->changed_data[$key]))
                                <span class="text-red-500" x-show="show=='changed'">{{ $change->changed_data[$key] === false ? '0' : $change->changed_data[$key]}}</span>
                                <div class="text-blue-500" x-show="show=='original'">
                                    {{ $change->data[$key] }}
                                </div>
                            @else
                                {{ $change->data[$key] }}
                            @endif
                        </x-td>
                        @endforeach
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
        {!! $changes->links() !!}
    </div>
    @else
        @if ($diff->status === 'expanded')
            <x-p>No User Data Changes to show.</x-p>
        @else
            <x-p>This Diff has not been expanded yet.</x-p>
        @endif
    @endif
@endsection
