@extends('layouts.wrapper', [
    'pageTitle' => 'User Feed Diffs | Index'
])

@section('content')
    {!! Breadcrumbs::render('user-feed.diffs') !!}
    <x-h1>User Data diffs</x-h1>
    <x-p>Shows all current (un-pruned) User Data Diffs fetched by the User Feed for this application</x-p>
    <x-hr />
    @if ($diffs->isNotEmpty())
        <x-table>
            <x-slot name="th">
                <x-th>ID</x-th>
                <x-th>Old Batch</x-th>
                <x-th>New Batch</x-th>
                <x-th>Total Changes</x-th>
                <x-th>Encrypted?</x-th>
                <x-th>Status</x-th>
                <x-th>Created At</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($diffs as $diff)
                    <x-tr>
                        <x-td>{{ $diff->id }}</x-td>
                        <x-td>
                            @if (!is_null($diff->old_batch_id))
                                <x-a href="{{ route('user-feed.batches.show', $diff->old_batch_id) }}">{{ $diff->old_batch_id }}</x-a>
                            @endif
                        </x-td>
                        <x-td>
                            @if (!is_null($diff->new_batch_id))
                                <x-a href="{{ route('user-feed.batches.show', $diff->new_batch_id) }}">{{ $diff->new_batch_id }}</x-a>
                            @endif
                        </x-td>
                        <x-td>{{ $diff->metrics['total_changes'] }}</x-td>
                        <x-td>{{ $diff->encrypted ? 'yes' : 'no' }}</x-td>
                        <x-td><span class="{{ $diff->statusClass }}"><i class="{{ $diff->statusIcon }}"></i> {{ $diff->status }}</span></x-td>
                        <x-td>{{ $diff->created_at }}</x-td>
                        <x-td>
                            <x-button color="blue" size="sm" href="{{ route('user-feed.diffs.show', $diff) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
        {!! $diffs->links() !!}
    @else
        <x-p>No Diffs to show.</x-p>
    @endif
@endsection
