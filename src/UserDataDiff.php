<?php

namespace Uncgits\Ccps\UserFeed;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Uncgits\Ccps\UserFeed\Events\UserDataDiffCreating;
use Uncgits\Ccps\UserFeed\Traits\HasStatusClassAndIcon;

class UserDataDiff extends Model
{
    use HasStatusClassAndIcon;

    protected $table = 'ccps_userfeed_user_data_diffs';
    protected $guarded = [];
    public $incrementing = false;

    protected $casts = [
        'metrics' => 'json'
    ];

    // events

    protected $dispatchesEvents = [
        'creating' => UserDataDiffCreating::class,
    ];

    // mutators

    public function setDataAttribute($value)
    {
        $encrypt = config('ccps-user-feed.encrypt_user_data');
        if ($encrypt) {
            $data = [
                'new_not_old' => encrypt($value['new_not_old']),
                'old_not_new' => encrypt($value['old_not_new']),
                'changed'     => encrypt($value['changed'])
            ];
        } else {
            $data = [
                'new_not_old' => $value['new_not_old'],
                'old_not_new' => $value['old_not_new'],
                'changed'     => $value['changed']
            ];
        }

        $this->attributes['data'] = json_encode($data);
        $this->setAttribute('encrypted', $encrypt);
    }

    // accessors

    public function getDataAttribute($value)
    {
        $data = json_decode($value, true);

        // base decryption off of whether this particular record was encrypted, not app setting.
        if ($this->encrypted) {
            $data = [
                'new_not_old' => decrypt($data['new_not_old']),
                'old_not_new' => decrypt($data['old_not_new']),
                'changed'     => decrypt($data['changed']),
            ];
        }

        return $data;
    }

    // relationships

    public function old_batch()
    {
        return $this->belongsTo(Batch::class, 'old_batch_id', 'id');
    }

    public function new_batch()
    {
        return $this->belongsTo(Batch::class, 'new_batch_id', 'id');
    }

    public function user_data_changes()
    {
        return $this->hasMany(UserDataChange::class);
    }

    // scopes

    public function scopeLatestExpanded($query)
    {
        return $query->where('status', 'expanded')->with('user_data_changes')->latest()->first();
    }

    public function scopeLatestChanges($query)
    {
        return $query->latestExpanded()->user_data_changes;
    }

    // generate a uuid

    public function setUuid()
    {
        $uuid = Uuid::uuid1()->toString();
        $this->setAttribute('id', $uuid);
    }
}
