<?php

namespace Uncgits\Ccps\UserFeed;

use Illuminate\Database\Eloquent\Model;
use Uncgits\Ccps\UserFeed\Events\UserDataSaving;

class UserData extends Model
{
    protected $table = 'ccps_userfeed_user_data';
    protected $guarded = [];

    // public $dataDecoded;

    // ELOQUENT EVENTS

    protected $dispatchesEvents = [
        'saving' => UserDataSaving::class,
    ];


    public function __get($key)
    {
        // need to override parent behavior
        return $this->getAttribute($key) ?? ($this->data->$key ?? null);
    }

    // relationships

    public function batch()
    {
        return $this->belongsTo(Batch::class);
    }

    // mutators

    public function setDataAttribute($value)
    {
        $encrypt = config('ccps-user-feed.encrypt_user_data');
        if ($encrypt) {
            $value = encrypt($value);
        } else {
            $value = json_encode($value);
        }

        $this->attributes['data'] = $value;
        $this->setAttribute('encrypted', $encrypt);
    }

    // accessors

    public function getDataAttribute($value)
    {
        // base decryption off of whether this particular record was encrypted, not app setting.
        if ($this->encrypted) {
            return decrypt($value);
        } else {
            return json_decode($value, true);
        }

        return $value;
    }


    // generate a hash based on the user's data

    public function setHash()
    {
        $hash = md5(serialize($this->data));
        $this->setAttribute('hash', $hash);
    }

    // RELATIONSHIPS

    public function alarms()
    {
        return $this->morphToMany(Alarm::class, 'alarmable');
    }
}
