<?php

namespace Uncgits\Ccps\UserFeed;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Uncgits\Ccps\UserFeed\Events\BatchCreating;
use Uncgits\Ccps\UserFeed\Traits\HasStatusClassAndIcon;

class Batch extends Model
{
    use HasStatusClassAndIcon;

    protected $table = 'ccps_userfeed_batches';
    protected $guarded = [];
    public $incrementing = false;

    protected $casts = [
        'metrics' => 'json'
    ];

    // events

    protected $dispatchesEvents = [
        'creating' => BatchCreating::class,
    ];

    // mutators

    public function setDataAttribute($value)
    {
        $encrypt = config('ccps-user-feed.encrypt_user_data');
        if ($encrypt) {
            $data = [
                'source' => encrypt($value['source']),
            ];
        } else {
            $data = [
                'source' => $value['source'],
            ];
        }

        $this->attributes['data'] = json_encode($data);
        $this->setAttribute('encrypted', $encrypt);
    }

    // accessors

    public function getDataAttribute($value)
    {
        $data = json_decode($value, true);

        // base decryption off of whether this particular record was encrypted, not app setting.
        if ($this->encrypted) {
            $data = [
                'source' => decrypt($data['source']),
            ];
        }

        return $data;
    }

    // relationships

    public function user_data()
    {
        return $this->hasMany(UserData::class);
    }

    public function diff_as_old_batch()
    {
        return $this->hasOne(UserDataDiff::class, 'old_batch_id');
    }

    public function diff_as_new_batch()
    {
        return $this->hasOne(UserDataDiff::class, 'new_batch_id');
    }

    // scopes

    private function getLatestWithStatus($query, $status)
    {
        return $query->where('status', $status)->with('user_data')->latest()->first();
    }

    public function scopeLatestExpanded($query)
    {
        return $this->getLatestWithStatus($query, 'expanded');
    }

    public function scopeLatestDiffed($query)
    {
        return $this->getLatestWithStatus($query, 'diffed');
    }

    public function scopeLatestData($query)
    {
        return $query->latestExpanded()->user_data;
    }

    // generate a uuid

    public function setUuid()
    {
        $uuid = Uuid::uuid1()->toString();
        $this->setAttribute('id', $uuid);
    }
}
