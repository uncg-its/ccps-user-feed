<?php

namespace Uncgits\Ccps\UserFeed;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Uncgits\Ccps\UserFeed\Events\UserDataChangeCreating;

class UserDataChange extends Model
{
    protected $table = 'ccps_userfeed_user_data_changes';
    protected $guarded = [];
    public $incrementing = false;

    // ELOQUENT EVENTS

    protected $dispatchesEvents = [
        'creating' => UserDataChangeCreating::class,
    ];

    // mutators

    public function setDataAttribute($value)
    {
        $encrypt = config('ccps-user-feed.encrypt_user_data');
        if ($encrypt) {
            $value = encrypt($value);
        } else {
            $value = json_encode($value);
        }

        $this->attributes['data'] = $value;
        $this->setAttribute('encrypted', $encrypt);
    }

    public function setChangedDataAttribute($value)
    {
        $encrypt = config('ccps-user-feed.encrypt_user_data');
        if ($encrypt) {
            $value = encrypt($value);
        } else {
            $value = json_encode($value);
        }

        $this->attributes['changed_data'] = $value;
        $this->setAttribute('encrypted', $encrypt);
    }

    // accessors

    public function getDataAttribute($value)
    {
        // base decryption off of whether this particular record was encrypted, not app setting.
        if ($this->encrypted) {
            return decrypt($value);
        } else {
            return json_decode($value, true);
        }

        return $value;
    }

    public function getChangedDataAttribute($value)
    {
        // base decryption off of whether this particular record was encrypted, not app setting.
        if ($this->encrypted) {
            return decrypt($value);
        } else {
            return json_decode($value, true);
        }

        return $value;
    }

    // relationships

    public function user_data_diff()
    {
        return $this->belongsTo(UserDataDiff::class, 'user_data_diff_id', 'id');
    }

    // generate a uuid

    public function setUuid()
    {
        $uuid = Uuid::uuid1()->toString();
        $this->setAttribute('id', $uuid);
    }
}
