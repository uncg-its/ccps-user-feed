<?php

namespace Uncgits\Ccps\UserFeed;

use App\CcpsCore\CronjobMeta;

abstract class BaseAlarm implements AlarmInterface
{
    protected $cronjobClassesToStop = [

    ];

    protected function stopCronjobs()
    {
        if (count($this->cronjobClassesToStop) > 0) {
            CronjobMeta::whereIn('class', $this->cronjobClassesToStop)->update(['status' => 'disabled']);
            \Log::channel('cron')->info('Cronjobs stopped from failed alarm: ' . implode(
                ', ',
                    $this->cronjobClassesToStop
            ));
        }
    }

    // actions to take if this Alarm check passes
    public function onPass($data)
    {
    }

    // actions to take if this Alarm check is failed
    public function onFail($data)
    {
    }
}
