<?php


namespace Uncgits\Ccps\UserFeed\Cronjobs;

use Uncgits\Ccps\Models\Cronjob;

abstract class UserFeedCronjob extends Cronjob
{
    protected $logChannel;

    public function __construct()
    {
        $this->logChannel = config('ccps-user-feed.log_channel');
        parent::__construct();
    }

    protected function log($message, $severity = 'info')
    {
        \Log::channel($this->logChannel)->$severity($message);
    }

    protected function setPhpOverrides()
    {
        if (!is_null($phpTempMemoryLimit = config('ccps-user-feed.php.temp_memory_limit'))) {
            ini_set('memory_limit', $phpTempMemoryLimit);
            $this->log('PHP Memory Limit overridden to: ' . $phpTempMemoryLimit, 'debug');
        }

        if (!is_null($phpTempMaxExecutionTime = config('ccps-user-feed.php.temp_max_execution_time'))) {
            ini_set('max_execution_time', $phpTempMaxExecutionTime);
            $this->log('PHP Execution Time overridden to: ' . $phpTempMaxExecutionTime, 'debug');
        }
    }
}
