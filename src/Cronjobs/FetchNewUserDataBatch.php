<?php

namespace Uncgits\Ccps\UserFeed\Cronjobs;

use Carbon\Carbon;
use Uncgits\Ccps\UserFeed\Batch;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\UserFeed\Traits\ChecksAlarms;
use Uncgits\GrouperApi\Exceptions\GroupNotFoundException;

class FetchNewUserDataBatch extends UserFeedCronjob
{
    use ChecksAlarms;

    protected $schedule = '* * * * *'; // default schedule (overridable in database)
    protected $display_name = 'Fetch New User Data Batch'; // default display name (overridable in database)
    protected $description = 'Fetches updated user information from Source and stores new Batch in the database for processing.'; // default description name (overridable in database)
    protected $tags = ['ccps-user-feed']; // default tags (overridable in database)

    protected function execute()
    {
        try {
            $this->setPhpOverrides();

            // pre-checks:
            $heldBatches = Batch::where('status', 'held')->get();
            if ($heldBatches->count() > 0) {
                $this->log(
                    '1 or more Batches with status of "held" detected. Cannot proceed with a new Batch until this is resolved.',
                    'error'
                );
                return new CronjobResult(true);
            }

            $start = Carbon::now();

            // Grab source from Grouper

            // define primary entity_id
            $entityId = config('ccps-user-feed.grouper.entity_id_field');

            // what else do we want to grab from Grouper to put in the user data?
            $subjectAttributeNames = config('ccps-user-feed.grouper.user_feed_fields');
            $this->log('Using Grouper fields: ' . implode(',', $subjectAttributeNames), 'debug');

            // add entity_id field, since we need that
            if (!in_array($entityId, $subjectAttributeNames)) {
                $subjectAttributeNames = array_merge($subjectAttributeNames, [$entityId]);
            }

            // add cn, since we need to ensure that we match off of it
            if (!in_array('cn', $subjectAttributeNames)) {
                $this->log('Note: cn field not included in Grouper field list. Adding manually.', 'debug');
                $subjectAttributeNames = array_merge($subjectAttributeNames, ['cn']);
            }

            // append 'mail' since it's returned last no matter what
            if (!in_array('mail', $subjectAttributeNames)) {
                $this->log('Note: appending \'mail\' attribute', 'debug');
                $subjectAttributeNames[] = 'mail';
            }

            // get Grouper records
            $grouperRecords = [];
            foreach (config('ccps-user-feed.grouper.source_groups') as $sourceGroup) {
                $this->log('Fetching membership for: ' . $sourceGroup, 'debug');

                // sometimes Grouper returns an invalid result - we should do due diligence to try and mitigate this by retrying a few times.
                $tries = 0;
                do {
                    $memberQuery = \Grouper::withSubjectAttributeNames($subjectAttributeNames)
                        ->getAllMembers($sourceGroup);

                    $tries++;

                    // testing
                    // unset($memberQuery[$sourceGroup]);
                } while (!isset($memberQuery[$sourceGroup]) && $tries < config('ccps-user-feed.fetch_tries_per_group'));

                if (isset($memberQuery[$sourceGroup])) {
                    $this->log('Fetched membership for ' . $sourceGroup . '. Tries: ' . $tries, 'info');
                } else {
                    $this->log('Membership query result empty for ' . $sourceGroup . '. Tries: ' . $tries, 'error');
                    throw new \Exception('Membership query result for ' . $sourceGroup . ' failed');
                }

                $grouperRecords = collect($memberQuery[$sourceGroup])
                    ->filter(function ($member) use ($subjectAttributeNames) {
                        // filter out groups
                        if (!in_array($member->sourceId, config('grouper-api.person_identifiers'))) {
                            return false;
                        }
                        // filter out anyone who has blank required fields
                        foreach (config('ccps-user-feed.required_nonblank_fields', []) as $field) {
                            $recordFields = array_combine($subjectAttributeNames, $member->attributeValues);
                            if (isset($recordFields[$field]) && $recordFields[$field] === '') {
                                return false;
                            }
                        }
                        return true;
                    })->values()
                    ->toArray();
            }

            $this->log('Fetched ' . count($grouperRecords) . ' records from Grouper.', 'debug');

            $memoryUsedMib = memory_get_peak_usage(false) / 1024 / 1024;

            $end = Carbon::now();

            \DB::beginTransaction();

            // generate batch
            $batch = Batch::create([
                'metrics' => [
                    'source_records_count'  => count($grouperRecords),
                    'source_fields'         => $subjectAttributeNames,
                    'memory_used_mib'       => $memoryUsedMib,
                    'time_taken_in_seconds' => $start->diffInSeconds($end),
                ],
                // data will be json_encoded at the Model level and encrypted if the app config says so
                'data'    => [
                    'source' => $grouperRecords,
                ],
                'status'  => 'pending',
            ]);

            $this->log('Batch ID ' . $batch->id . ' created. Metrics: ' . json_encode($batch->metrics), 'debug');

            // check alarms

            // pass in Batch
            $this->checkAlarms($batch);

            // if we haven't alarmed-out yet, set to approved.

            if ($batch->status != 'held') {
                $this->log('Batch not held by Alarm checks. Approving.', 'debug');
                $batch->status = 'approved';
            }

            $batch->save(); // save either 'held' or 'approved' status

            \DB::commit();

            return new CronjobResult(true);
        } catch (GroupNotFoundException $e) {
            $this->log($e->getMessage(), 'warning');
            return new CronjobResult(false, $e->getMessage());
        } catch (\Exception $e) {
            // roll back database changes
            \DB::rollBack();
            $this->log(
                'Exception during cronjob ' . __CLASS__ . ': ' . $e->getMessage() . '. Rethrowing Exception to be handled further.',
                'error'
            );
            // re-throw exception
            throw $e;
        }
    }
}
