<?php

namespace Uncgits\Ccps\UserFeed\Cronjobs;

use Uncgits\Ccps\UserFeed\Batch;
use Uncgits\Ccps\UserFeed\UserData;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\UserFeed\Traits\ChecksAlarms;

class ExpandUserDataFromBatch extends UserFeedCronjob
{
    use ChecksAlarms;

    protected $schedule = '* * * * *'; // default schedule (overridable in database)
    protected $display_name = 'Expand User Data From Batch'; // default display name (overridable in database)
    protected $description = 'Expands latest approved Batch and generates individual user data in the User Data table.'; // default description name (overridable in database)
    protected $tags = ['ccps-user-feed']; // default tags (overridable in database)

    protected $idsAdded;

    protected function execute()
    {
        try {
            $this->setPhpOverrides();

            // define primary entity_id
            $entityId = config('ccps-user-feed.grouper.entity_id_field');

            // what else do we want to grab from Grouper to put in the user data?
            $subjectAttributeNames = config('grouper-api.subject_attribute_names');

            // STEP 1 - grab the latest batch that's in an "approved" state.
            $batch = Batch::where('status', 'approved')->orderBy('created_at', 'asc')->first();

            if (is_null($batch)) {
                $this->log(__CLASS__ . ' cronjob found no approved User Data batch. Nothing to do.');
                return new CronjobResult(true);
            }

            $this->log('Expanding Batch ' . $batch->id, 'debug');

            // STEP 2 - get the data out of it
            $sourceData = (array)$batch->data['source'];

            // STEP 3 - compile

            // parse and add
            $idsAdded = [];

            \DB::beginTransaction();

            foreach ($sourceData as $member) {
                // map subject attribute names so that we know what we got
                // 'mail' is appended automatically as the last element if it was not asked for.
                $subjectAttributeKeys = in_array('mail', $subjectAttributeNames) ?
                    $subjectAttributeNames :
                    array_merge($subjectAttributeNames, ['mail']);

                $userData = array_combine($subjectAttributeKeys, data_get($member, 'attributeValues'));

                // drop fields if needed
                foreach (config('ccps-user-feed.dropped_fields') as $field) {
                    if (isset($userData[$field])) {
                        unset($userData[$field]);
                    }
                }

                $userData['exists_in_source'] = true;

                $entityIdValue = $userData[$entityId];

                // do not add if we have one in there already for that user.
                if (!in_array($entityIdValue, $idsAdded)) {
                    $insertArray = [
                        'batch_id'  => $batch->id,
                        'entity_id' => $entityIdValue,
                        'data'      => $userData
                    ];

                    $userData = UserData::create($insertArray);

                    $idsAdded[] = $entityIdValue;
                }
            }


            $batch->status = 'expanded';
            $batch->save();

            $this->idsAdded = $idsAdded;
            $this->checkAlarms([
                'batch'       => $batch,
                'idsExpanded' => $this->idsAdded
            ]);

            // commit DB changes
            \DB::commit();

            return new CronjobResult(true);
        } catch (\Throwable $e) {
            // roll back DB changes
            \DB::rollBack();

            $this->log(
                'Exception during cronjob ' . __CLASS__ . ': ' . $e->getMessage() . '. Rethrowing Exception to be handled further.',
                'error'
            );
            // re-throw exception
            throw $e;
        }
    }
}
