<?php

namespace Uncgits\Ccps\UserFeed\Cronjobs;

use Uncgits\Ccps\UserFeed\Batch;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\UserFeed\UserDataDiff;
use Uncgits\Ccps\UserFeed\UserDataChange;
use Uncgits\Ccps\UserFeed\Traits\ChecksAlarms;

class ExpandUserDataChangesFromDiff extends UserFeedCronjob
{
    use ChecksAlarms;

    protected $schedule = '* * * * *'; // default schedule (overridable in database)
    protected $display_name = 'Expand User Data Changes From Diff'; // default display name (overridable in database)
    protected $description = 'Expands the latest diff into User Data Changes'; // default description name (overridable in database)
    protected $tags = ['ccps-user-feed']; // default tags (overridable in database)

    protected $userDataDiff;

    protected function execute()
    {
        try {
            $this->setPhpOverrides();

            // PREP
            // pre-checks:
            $allDiffs = UserDataDiff::orderBy('created_at', 'asc')->get();

            if ($allDiffs->where('status', 'held')->count() > 0) {
                $this->log(
                    '1 or more Diffs with status of "held" detected. Cannot proceed with Diff expansion until this is resolved.',
                    'error'
                );
                return new CronjobResult(true);
            }

            // STEP 1 - find earliest ready diff for expansion
            $diffToExpand = $allDiffs->sortBy('created_at')->where('status', 'approved')->first();
            if (is_null($diffToExpand)) {
                $this->log(__CLASS__ . ' cronjob found no approved User Data Diff. Nothing to do.');
                return new CronjobResult(true);
            }

            $this->log('Expanding Diff ' . $diffToExpand->id, 'debug');

            $createdChanges = [];

            collect($diffToExpand->data)->each(function ($item) use (&$createdChanges, $diffToExpand) {
                collect($item)->each(function ($change) use (&$createdChanges, $diffToExpand) {
                    $change['user_data_diff_id'] = $diffToExpand->id;
                    $createdChanges[] = UserDataChange::create($change);
                });
            });

            $this->log('Expanded ' . count($createdChanges) . ' user data diff records.', 'debug');


            // check all applicable alarms
            $this->checkAlarms([
                'userDataDiff'   => $diffToExpand,
                'createdChanges' => $createdChanges
            ]);

            $diffToExpand->status = 'expanded';
            $diffToExpand->save(); // save 'expanded' status

            if (!is_null($diffToExpand->old_batch_id)) {
                $oldBatch = Batch::find($diffToExpand->old_batch_id);
                $oldBatch->status = 'diffed';
                $oldBatch->save();
            }

            $newBatch = Batch::find($diffToExpand->new_batch_id);
            $newBatch->status = 'diffed';
            $newBatch->save();

            \DB::commit();

            return new CronjobResult(true);
        } catch (\Exception $e) {
            \DB::rollBack();

            $this->log(
                'Exception during cronjob ' . __CLASS__ . ': ' . $e->getMessage() . '. Rethrowing Exception to be handled further.',
                'error'
            );
            // re-throw exception
            throw $e;
        }
    }
}
