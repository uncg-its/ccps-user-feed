<?php

namespace Uncgits\Ccps\UserFeed\Cronjobs;

use Carbon\Carbon;
use Uncgits\Ccps\UserFeed\Batch;
use Uncgits\Ccps\UserFeed\UserData;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\UserFeed\UserDataDiff;
use Uncgits\Ccps\UserFeed\Traits\ChecksAlarms;

class CreateUserDataDiff extends UserFeedCronjob
{
    use ChecksAlarms;

    protected $schedule = '* * * * *'; // default schedule (overridable in database)
    protected $display_name = 'Create User Data Diff'; // default display name (overridable in database)
    protected $description = 'Creates a user data diff from the last two batches of expanded user data'; // default description name (overridable in database)
    protected $tags = ['ccps-user-feed']; // default tags (overridable in database)

    protected function execute()
    {
        try {
            $this->setPhpOverrides();

            // PREP
            // pre-checks:
            $heldDiffs = UserDataDiff::where('status', 'held')->get();
            if ($heldDiffs->count() > 0) {
                $this->log(
                    '1 or more Diffs with status of "held" detected. Cannot proceed with a new Diff until this is resolved.',
                    'error'
                );
                return new CronjobResult(true);
            }

            $start = Carbon::now();

            $metrics = [
                'old_not_new_count'  => 0,
                'new_not_old_count'  => 0,
                'changed_user_count' => 0,
                'total_changes'      => 0,
            ];

            \DB::beginTransaction();

            // STEP 1 - get the target batches. one is from the latest known good diff, the other is the earliest expanded batch we have.
            $batches = Batch::all();
            $diffs = UserDataDiff::all();

            // do we even have enough Batches created? this should only affect the first run, but will catch cases when the database may have been truncated for some reason.
            if ($batches->count() == 0) {
                // we need at least one batch to generate a diff. if we get here, we don't have that many.
                $this->log('Cannot diff without at least 1 batch. Exiting.');
                return new CronjobResult(true);
            }

            // if we have enough Batches fundamentally, we need at least one of those batches to be 'expanded'. grab the first one.
            $expandedBatches = $batches->where('status', 'expanded')->sortBy('created_at');
            $newBatch = $expandedBatches->first();
            if (is_null($newBatch) || empty($newBatch)) {
                $this->log('No expanded batches exist for diffing. Exiting.');
                return new CronjobResult(true);
            }


            // we have an expanded batch. now, we need to find the old batch
            // ideally, this is the "new" batch in the most recent last-known-good diff.

            // first we need to make sure we have a last-known-good diff.

            // if we have no diffs at all, we need to have two 'expanded' batches to diff (one if it's the initial diff)
            if ($diffs->count() == 0) {
                $this->log('No existing diffs found. Must create initial diff.');

                // the new batch is OK as we grabbed it above. first batch should be diffed against nothing.
                $oldBatch = null;
            } else {
                // we need at least one diff of status "expanded" to proceed.
                $expandedDiffs = $diffs->where('status', 'expanded')->sortByDesc('created_at');
                if ($expandedDiffs->count() < 1) {
                    $this->log('No expanded diffs found. Cannot locate last-known-good diff. Exiting');
                    return new CronjobResult(true);
                }

                $lastKnownGoodDiff = $expandedDiffs->first();
                $oldBatch = $batches->firstWhere('id', $lastKnownGoodDiff->new_batch_id);

                if ($newBatch->created_at < $oldBatch->created_at) {
                    $message = 'Logical error: new batch should not have been created before old batch! Exiting as a precaution.';
                    $this->log($message, 'error');
                    return new CronjobResult(false, $message);
                }

                if ($newBatch->id == $oldBatch->id) {
                    $message = 'Logical error: new batch and old batch should not have same id! Exiting as a precaution.';
                    $this->log($message, 'error');
                    return new CronjobResult(false, $message);
                }
            }

            $oldBatchId = optional($oldBatch)->id;
            $oldBatchIdString = optional($oldBatch)->id ?? 'none';

            // STEP 2 - see if we already have a diff for these two batches
            $matchingDiffs = UserDataDiff::where('old_batch_id', $oldBatchId)
                ->where('new_batch_id', $newBatch->id)
                ->whereIn('status', ['approved', 'expanded'])
                ->first();

            if (!is_null($matchingDiffs)) {
                $this->log("Already have an approved or expanded diff between batches $newBatch->id and $oldBatchIdString. Exiting.");
                return new CronjobResult(true);
            }

            // STEP 3 - determine what needs to be marked as changed

            // NOTE - initial diff ($userDataDiff->old_batch_id == null) will return an empty collection. This works.

            $this->log("Starting user data diff. Old batch: $oldBatchIdString; New batch: $newBatch->id");

            $oldUserData = UserData::where('batch_id', $oldBatchId)->get()->keyBy('entity_id');
            $newUserData = UserData::where('batch_id', $newBatch->id)->get()->keyBy('entity_id');

            // in old source but not new
            $inOldNotNew = $oldUserData->diffKeys($newUserData);
            $metrics['old_not_new_count'] = $inOldNotNew->count();
            // in new source but not old
            $inNewNotOld = $newUserData->diffKeys($oldUserData);
            $metrics['new_not_old_count'] = $inNewNotOld->count();

            // existing users that have changed
            $inBothOld = $oldUserData->intersectByKeys($newUserData);
            $inBothNew = $newUserData->intersectByKeys($oldUserData);

            if (is_null($oldBatch) || $newBatch->metrics['source_fields'] != $oldBatch->metrics['source_fields']) {
                // the fields have changed. we need to trigger all as changed anyway.
                $changedUsers = $inBothOld;
            } else {
                // figure out what specific records are different
                $changedUsers = collect([]);

                $inBothOld->each(function ($item, $key) use ($inBothNew, $changedUsers) {
                    $oldHash = $item->hash;
                    $newHash = $inBothNew[$key]->hash;

                    if ($oldHash != $newHash) {
                        $changedUsers->put($key, $item);
                    }
                });
            }

            $metrics['changed_user_count'] = $changedUsers->count();
            $metrics['total_changes'] = array_sum($metrics);

            $diffData = [
                'old_not_new' => [],
                'new_not_old' => [],
                'changed'     => []
            ];

            $userDataDiff = UserDataDiff::create([
                'old_batch_id' => $oldBatchId,
                'new_batch_id' => $newBatch->id,
                'metrics'      => '',
                'data'         => $diffData,
                'status'       => 'pending',
            ]);

            // ADDS - set $changed_data->exists_in_source = true
            $inNewNotOld->each(function ($item, $key) use ($oldBatch, $newBatch, &$diffData, $userDataDiff) {
                $changedData = array_merge([
                    'exists_in_source' => true
                ], $item->data);

                $diffData['new_not_old'][] = [
                    'user_data_diff_id' => $userDataDiff->id,
                    'data'              => $item->data,
                    'changed_data'      => $changedData,
                ];
            });


            // REMOVES - set $changed_data->exists_in_source = false
            $inOldNotNew->each(function ($item, $key) use ($oldBatch, $newBatch, &$diffData, $userDataDiff) {
                $changedData = [
                    'exists_in_source' => false
                ];

                $diffData['old_not_new'][] = [
                    'user_data_diff_id' => $userDataDiff->id,
                    'data'              => $item->data,
                    'changed_data'      => $changedData,
                ];
            });

            // CHANGES - set $changed_data attributes based on what's changed
            $changedUsers->each(function ($item, $key) use (
                $inBothNew,
                $oldBatch,
                $newBatch,
                &$diffData,
                $userDataDiff
            ) {
                $oldData = $item->data;
                $newData = $inBothNew[$key]->data;

                $changedData = [];
                foreach ($oldData as $k => $v) {
                    // ignore changes if field should be ignored.
                    if (in_array($k, config('ccps-user-feed.ignored_fields'))) {
                        continue;
                    }

                    if (isset($newData[$k])) {
                        if ($v != $newData[$k]) {
                            // if prop exists and different from new data
                            $changedData[$k] = $newData[$k];
                        }
                    } else {
                        // if prop doesn't exist in new data structure at all
                        $changedData[$k] = null;
                    }
                }

                foreach ($newData as $k => $v) {
                    // ignore changes if field should be ignored.
                    if (in_array($k, config('ccps-user-feed.ignored_fields'))) {
                        continue;
                    }

                    if (!isset($oldData[$k])) {
                        // if prop is new in the new data structure
                        $changedData[$k] = $v;
                    }
                }

                if ($changedData !== []) {
                    $diffData['changed'][] = [
                        'user_data_diff_id' => $userDataDiff->id,
                        'data'              => $newData,
                        'changed_data'      => $changedData,
                    ];
                }
            });

            $end = Carbon::now();

            $metrics['time_taken_in_seconds'] = $start->diffInSeconds($end);

            $userDataDiff->update([
                'metrics' => $metrics,
                'data'    => $diffData,
            ]);

            $this->log('Diff ID ' . $userDataDiff->id . ' created. Metrics: ' . json_encode($metrics), 'debug');

            // check all applicable alarms
            $this->checkAlarms($userDataDiff);

            if ($userDataDiff->status != 'held') {
                $this->log('Diff not held by Alarm checks. Approving.', 'debug');
                $userDataDiff->status = 'approved';
            }

            $userDataDiff->save(); // save either 'held' or 'approved' status

            \DB::commit();

            return new CronjobResult(true);
        } catch (\Exception $e) {
            \DB::rollBack();

            $this->log(
                'Exception during cronjob ' . __CLASS__ . ': ' . $e->getMessage() . '. Rethrowing Exception to be handled further.',
                'error'
            );
            // re-throw exception
            throw $e;
        }
    }
}
