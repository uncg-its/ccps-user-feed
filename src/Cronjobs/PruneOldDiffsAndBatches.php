<?php

namespace Uncgits\Ccps\UserFeed\Cronjobs;

use Uncgits\Ccps\UserFeed\Batch;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\UserFeed\UserDataDiff;
use Uncgits\Ccps\UserFeed\Traits\ChecksAlarms;

class PruneOldDiffsAndBatches extends UserFeedCronjob
{
    use ChecksAlarms;

    protected $schedule = '* * * * *'; // default schedule (overridable in database)
    protected $display_name = 'Prune Old Diffs and Batches'; // default display name (overridable in database)
    protected $description = 'Cleans up diff, batch, and user data according to the number of kept diffs in the config'; // default description name (overridable in database)
    protected $tags = ['ccps-user-feed']; // default tags (overridable in database)

    protected $userDataDiff;

    protected function execute()
    {
        try {
            $this->setPhpOverrides();

            // STEP 1 - fetch latest X diffs to determine which batches are relevant to us
            $diffsToKeep = config('ccps-user-feed.diffs_to_keep');
            if ($diffsToKeep < 1) {
                $message = 'App must be set to keep at least 1 diff. Invalid value: ' . $diffsToKeep;
                $this->log($message, 'error');
                return new CronjobResult(false, $message);
            }

            $this->log('Application set to keep ' . $diffsToKeep . ' ' . \Str::plural('diff', $diffsToKeep), 'debug');

            $allDiffs = UserDataDiff::select('id', 'old_batch_id', 'new_batch_id', 'status', 'created_at')
                ->where('status', 'expanded')
                ->orderByDesc('created_at')
                ->get();

            if ($allDiffs->count() <= $diffsToKeep) {
                $this->log('Diff count does not exceed kept-diffs count. No need to prune');
                return new CronjobResult(true);
            }

            $diffsToRemove = $allDiffs->slice($diffsToKeep);
            $this->log('Removing diff(s): ' . $diffsToRemove->pluck('id')->implode(', '), 'debug');

            \DB::beginTransaction();

            // store batch IDs for FK reasons
            $batchesToRemove = Batch::whereIn('id', $diffsToRemove->pluck('old_batch_id')->toArray())
                ->pluck('id');

            $diffsToRemove->each(function ($diff) {
                // cascades to changes
                $diff->delete();
            });

            // remove batches now
            $this->log('Removing associated batch(es): ' . $batchesToRemove->implode(', '), 'debug');
            Batch::whereIn('id', $batchesToRemove)->delete();

            $this->checkAlarms([
                'diffsToRemove'   => $diffsToRemove,
                'batchesToRemove' => $batchesToRemove
            ]);

            \DB::commit();

            return new CronjobResult(true);
        } catch (\Exception $e) {
            \DB::rollBack();

            $this->log(
                'Exception during cronjob ' . __CLASS__ . ': ' . $e->getMessage() . '. Rethrowing Exception to be handled further.',
                'error'
            );
            // re-throw exception
            throw $e;
        }
    }
}
