<?php

namespace Uncgits\Ccps\UserFeed\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

abstract class UserFeedAlarmEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $alarmClass;
    public $data;
    public $cronjobClass;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($alarmClass, $data, $cronjobClass)
    {
        $this->alarmClass = $alarmClass;
        $this->data = $data;
        $this->cronjobClass = $cronjobClass;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('ccps-user-feed');
    }
}
