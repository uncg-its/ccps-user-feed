<?php

namespace Uncgits\Ccps\UserFeed\Events;

use Uncgits\Ccps\UserFeed\UserData;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class UserDataSaving
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userData;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(UserData $userData)
    {
        $this->userData = $userData;
    }
}
