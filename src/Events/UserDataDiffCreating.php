<?php

namespace Uncgits\Ccps\UserFeed\Events;

use Illuminate\Queue\SerializesModels;
use Uncgits\Ccps\UserFeed\UserDataDiff;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class UserDataDiffCreating
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $diff;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(UserDataDiff $diff)
    {
        $this->diff = $diff;
    }
}
