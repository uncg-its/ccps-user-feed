<?php

namespace Uncgits\Ccps\UserFeed\Events;

use Illuminate\Queue\SerializesModels;
use Uncgits\Ccps\UserFeed\UserDataChange;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class UserDataChangeCreating
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $change;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(UserDataChange $change)
    {
        $this->change = $change;
    }
}
